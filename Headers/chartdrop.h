#ifndef CHARTDROP_H
#define CHARTDROP_H


#include <QtCharts>
#include <cassert>



class ChartDrop : public QChart
{
    Q_OBJECT

private:
  //  QLogValueAxis *axisX;
   // QValueAxis *axisY;

public:
    ChartDrop(bool b);

    void dropEvent(QGraphicsSceneDragDropEvent* event);
//    void resize();


    /*
    void createLinAxis(int Xmin, int Xmax, QValueAxis *axisX, QValueAxis *axisY);
    void addNewSeries(double *x, double *y, int l, QLineSeries *series);
    void createLogAxis(int Xmin, int Xmax, QLogValueAxis *axisX, QValueAxis *axisY);
    void attachAxis(QLineSeries *series, QAbstractAxis *axisX, QAbstractAxis *axisY);
//*/


signals:
    void newFileDropped(std::string str);




};



#endif // QCHARTDROP_H
