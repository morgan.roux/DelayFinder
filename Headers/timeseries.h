#ifndef TIMESERIES_H
#define TIMESERIES_H

#include <QtCharts>
#include "Headers/measurement.h"
#include <cassert>


class TimeSeries : public QLineSeries
{
    Q_OBJECT

public:
    TimeSeries();
    TimeSeries(double *x, double *y, int n);

    void newSeries(double *x, double *y, int n);

};

#endif // TIMESERIES_H
