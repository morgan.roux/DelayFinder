#ifndef DIALOGPARAM_H
#define DIALOGPARAM_H

#include <QDialog>
#include "Headers/mainwindow.h"
#include <cassert>


namespace Ui {
class DialogParam;
}

class DialogParam : public QDialog
{
    Q_OBJECT

public:
    explicit DialogParam(MainWindow *parent = 0);
    ~DialogParam();

    MainWindow *m_parent;


private:
    Ui::DialogParam *ui;

signals :
    void paramUpdate();

public slots :
    void updateparams();

};

#endif // DIALOGPARAM_H
