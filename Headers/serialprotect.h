#ifndef SERIALPROTECT_H
#define SERIALPROTECT_H

#include <QtNetwork>
#include "stdio.h"
#include "md5.h"
#include <string>
#include <QString>
#include <QSettings>


using namespace std;

class SerialProtect
{
public:
    SerialProtect();
    bool createSerial();
    bool isAuthorized();
    bool verifyKey(string sn, string key);
    QString getMacAddress();
    void writeKey(string strg);
    void unauthorize();
    void reactivateTrial();
    bool checkTrialPeriod();

    string getSerial() const;

private:
    string Serial;

    const int TrialPeriod = 15;         //number of days in Trial mode







};

#endif // SERIALPROTECT_H
