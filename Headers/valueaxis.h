#ifndef VALUEAXIS_H
#define VALUEAXIS_H

#include <QtCharts>
#include <cassert>

class ValueAxis : public QValueAxis
{
    Q_OBJECT
public:

    ValueAxis();
    double m_max;
    double m_min;

    void resize();

public slots:
    void applyNiceNumbers();
    void updateSize(qreal min, qreal max);
    void onRangeChanged(qreal min, qreal max);
};

#endif // VALUEAXIS_H
