#ifndef CHARTVIEW_H
#define CHARTVIEW_H

#include <QtCharts>
#include <cassert>
#include "valueaxis.h"

class ChartView : public QChartView
{
    Q_OBJECT
public:
    ChartView(QChart *chart);

    bool getMouseEnabled() const;
    void setMouseEnabled(bool value);

public slots:
    void keyPressEvent(QKeyEvent *event);
    void mouseDoubleClickEvent(QMouseEvent *event);

signals:
    void newXY(double x, double y);
  // void newXY(QString x, QString y);

protected:
   bool mouseEnabled = false;

};



class ChartViewTimeDomain : public ChartView
{
    Q_OBJECT

public:
    ChartViewTimeDomain(QChart *chart);
public slots:
    virtual void mouseMoveEvent(QMouseEvent *event);


};

class ChartViewFreqDomain : public ChartView
{
    Q_OBJECT

public:
    ChartViewFreqDomain(QChart *chart);
public slots:
    virtual void mouseMoveEvent(QMouseEvent *event);

};


#endif // CHARTVIEW_H
