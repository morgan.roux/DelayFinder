#ifndef FREQSERIES_H
#define FREQSERIES_H

#include <QtCharts>
#include "Headers/measurement.h"
#include <cassert>


class FreqSeries : public QSplineSeries
{
    Q_OBJECT

public:
    FreqSeries();
    void newMagSeries(TF *tf, double n, double fmin, double fmax);
    void newPhsSeries(TF *tf, int n, int fmin, int fmax);

};

#endif // FREQSERIES_H
