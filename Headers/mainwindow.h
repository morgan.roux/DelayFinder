#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QWidget>
#include <QtGui>
#include <QTCore>
#include <QPushButton>
#include <QLabel>
#include <QGridLayout>
#include <QGroupBox>
#include <QLabel>
#include <QChart>
#include <QPen>

#include "chartdrop.h"
#include "chartview.h"
#include "freqseries.h"
#include "timeseries.h"
#include "valueaxis.h"
#include "dialogabout.h"



//#include <sndfile.h>
#include <cassert>


#include "Headers/measurement.h"


#define PHASE_AUTO  0
#define PHASE_IN    1
#define PHASE_OUT   2


namespace Ui {
class MainWindow;
}


class MainWindow : public QMainWindow
{
    Q_OBJECT
public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

    int fs() const;
    void setFs(int fs);

    int fmax() const;
    void setFmax(int fmax);

    int fmin() const;
    void setFmin(int fmin);

    int phase() const;
    void setPhase(int phase);

    int mmin() const;
    void setMmin(int mmin);

    int mmax() const;
    void setMmax(int mmax);

private:
    Ui::MainWindow *ui;
    void initUI();
    void freememory();

//======== Ui ===========//
    //Fenetre prncipale
    QWidget *w;
    //Layout principal
    QGridLayout *lay;

    QPushButton *butGo;
    QLabel *lblDly;
    QLabel *lblInfo;
    QLabel *lbl_coordX;
    QLabel *lbl_coordY;

    //Organisation verticale
    //Partie gauche
    QVBoxLayout *vlay1;
    QVBoxLayout *vlay11;
    QVBoxLayout *vlay12;
    //Partie droite
    QVBoxLayout *vlay2;
    QVBoxLayout *vlay21;

    QGridLayout *hlay;
    //Groupes de widget
    //Impulses
    QGroupBox *grpImp;
    //Xcorr
    QGroupBox *grpXcorr;
    //Transfert Functions
    QGroupBox *grpTf;

    //Series
    //imp1,imp2
    TimeSeries *seriesImp1;
    TimeSeries *seriesImp2;
    //Xcorr
    TimeSeries *seriesXcorr;
    //Transfert Functions
    FreqSeries *seriesTf1;
    FreqSeries *seriesTf2;
    FreqSeries *seriesTf3;
    FreqSeries *seriesTf4;
    FreqSeries *seriesTf5;
    FreqSeries *seriesTf6;

    //Graphiques
    ChartDrop *chartImp1;
    ChartDrop *chartImp2;
    ChartDrop *chartXcorr;
    ChartDrop *chart4;
    ChartDrop *chart5;

    //Vues pour les graphiques
    ChartViewTimeDomain *chartViewImp1;
    ChartViewTimeDomain *chartViewImp2;
    ChartViewTimeDomain *chartViewXCorr3;
    QChartView *chartView4;
    ChartViewFreqDomain *chartView5;

    QLogValueAxis *axis4X, *axis5X;
    ValueAxis *axis1X, *axis1Y,
              *axis2X, *axis2Y,
              *axis3X, *axis3Y,
              *axis4Y,
              *axis5Y;

//===========================//

    //Impulses et fonction de transfert
    Impulse *imp1;    //fichier 1
    Impulse *imp2;      //fihier 2

    Impulse *imp4;      //fichier 1 après traitement
    Impulse *imp5;      //fichier 2 après traitement

    TF *tf1;      //FFT(imp1)
    TF *tf2;      //FFT(imp2)
    TF *tf3;      //tf1+tf2
    TF *tf4;      //FFT(imp4)
    TF *tf5;      //FFT(imp5)
    TF *tf6;      //tf4+tf5


protected:
    int m_fs = 0;       //Freq d'échantillonage
    int m_fmax = 100;
    int m_fmin = 20;
    int m_mmin = -30;
    int m_mmax = 10;

    int m_phase = PHASE_AUTO;    //0: auto - 1: in phase - 2: out of phase
    long dsamples = 0;
    Peak pk;
    double *xc;    //valeur du calcul de cross correlation
    double *lags;
    bool go = false;    //Mis à true lors du premier click sur go

    QChart::ChartTheme chartTheme = QChart::ChartThemeBlueIcy; //QChart::ChartThemeBlueCerulean;



public slots:
    void loadImpulse1(string strc);
    void loadImpulse2(string strc);
    void on_butGo_clicked();
    void updateXYTimeDomain(double xVal, double yVal);
    void updateXYFreqDomain(double xVal, double yVal);


private slots:
    void on_actionPref_triggered();
    void on_actionAbout_triggered();
    void on_actionMeasure_triggered();
};

#endif // MAINWINDOW_H

