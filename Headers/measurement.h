#ifndef MEASUREMENT_H
#define MEASUREMENT_H

#define SAMPLES_MAX 48000

#include <vector>
#include <string>
#include <sndfile.h>

#include <math.h>
#include <complex>
#include <fftw3.h>

#include "winmls.h"
#include "smaart.h"

#include <cassert>

#define I_complx std::complex<double>(0.0, 1.0)
const double PI = std::acos(-1);

struct Peak;
class Impulse;
class TF;
//class Measure;
//class Meausurement;


void xCorr(double* signala, double* signalb, double* result, int N);
void conv(double* signala, double* signalb, double* result, unsigned int lenghta, unsigned int lenghtb, int action);
void ifft(Impulse *imp, TF *tf);
void fft(Impulse *imp, TF *tf);

Peak findAbsolutePeak(double* signal, int n);
Peak findMaxPeak(double *signal, int n);
Peak findMinPeak(double *signal, int n);


struct Peak
{
    double max;
    long delta;
};

class Impulse
{
//##########
    //Ajuster type filename et initialisation

//private:


public :
    double *m_x=NULL;
    double *m_y=NULL;

    int m_Fs;
    long m_samples;
    long m_nfft;
    int m_winlen;
    string m_filename;

    void initData(unsigned long l);


public:
    Impulse(string filenm);
    Impulse(Impulse const& imp);
    Impulse(double* imp, unsigned int fs, unsigned int samples, unsigned int winlen );

    ~Impulse();

    void timeShift(long delay);
    void normalize();
    void window(int wLen);
    void antiphase();
    void sum(Impulse *imp);
    void clear();

    long samples() const;
    void setSamples( long samples);
    int winlen() const;
    void setWinlen(int winlen);
    string filename() const;
    double *getX() const;
    void setX(double *value);
    double *getY() const;
    void setY(double *value);
    int getFs() const;
    void setFs(int Fs);


};


class TF
{
    
private:
    int m_Fs;
    long m_samples;
    long m_nfft;
    double  m_Fprec;

    double *m_f;                //Axe fréquentiel
    double *m_mag;              //Magnitude
    double *m_phs;              //Phase
    std::complex<double> *m_cpx;    //représentation complexe

public:
    TF(unsigned short Fs, unsigned short samples);
    TF(TF const& tf);
    ~TF();
    
    void timeShift(int samples);
    void antiphase();
    void cpx2polar();

    void sum(TF *tf);
    void div(TF *tf);

    double Fprec() const;
    void setFprec(double Fprec);
    int Fs() const;
    void setFs(int Fs);
    std::complex<double> *cpx() const;
    void setCpx(std::complex<double> *cpx);
    double *f() const;
    void setF(double *f);
    double *mag() const;
    void setMag(double *mag);
    double *phs() const;
    void setPhs(double *phs);
    long samples() const;
    void setSamples(long samples);
    long nfft() const;
    void setNfft(long nfft);
};



/*=====================================================


class Measure
{
public:
    int m_Fs;

    Measure();
    ~Measure();


    double* getX();
    double* getY();

};

*/

/*
class Measurement
{


private:

    int     m_Fmin, m_Fmax;
    
public:
    vector<Impulse*>     m_impList;
    vector<TransFct*>    m_tfList;
    
  
    Measurement();
    ~Measurement();

    inline IMP_INDEX newImp(const char *flnm)
    {//On retourne l'index du nouvel élément
        Impulse* p = new Impulse();
        p->loadFromFile(flnm);
        
        this->m_impList.push_back(p);
        return (m_impList.size() - 1);
    }
    
    inline TF_INDEX newTF()
    {//On retourne l'index du nouvel élément
        this->m_tfList.push_back(new TransFct());
        return (m_impList.size() - 1);
    }
    
    inline void delImp(IMP_INDEX n)
    {
        delete this->m_impList[n];
        this->m_impList.pop_back();
    }
    
    inline void delTF(TF_INDEX n)
    {
        delete this->m_tfList[n];
        this->m_tfList.pop_back();
    }
    
    inline Impulse getImpulse(IMP_INDEX n)
    {
        return *m_impList.at(n);
    }
    

    inline TransFct getTransFct(TF_INDEX n)
    {
        return *m_tfList.at(n);
    }
    
    
};

*/

#endif // MEASUREMENT_H
