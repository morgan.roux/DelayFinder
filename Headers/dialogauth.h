#ifndef DIALOGAUTH_H
#define DIALOGAUTH_H

#include <QDialog>
#include <QMessageBox>
#include "serialprotect.h"

namespace Ui {
class DialogAuth;
}

class DialogAuth : public QDialog
{
    Q_OBJECT

public:
    explicit DialogAuth(QWidget *parent = 0, SerialProtect *serial = NULL);
    ~DialogAuth();

public slots:
    void on_buttonOk_click();
    void on_buttonTrial_click();

private:
    Ui::DialogAuth *ui;
    SerialProtect* srl;

};

#endif // DIALOGAUTH_H
