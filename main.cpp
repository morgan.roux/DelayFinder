#include <QApplication>
#include "Headers/mainwindow.h"
#include "Headers/dialogauth.h"
#include "Headers/serialprotect.h"
#include <QStyleFactory>
#include <QStringList>
#include <QString>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow *m = new MainWindow();
    SerialProtect *srl = new SerialProtect();
    DialogAuth *dlg;

    QCoreApplication::setOrganizationName("Wevo Audio");
    QCoreApplication::setOrganizationDomain("wevo-audio.fr");
    QCoreApplication::setApplicationName("In Phase");

    a.setApplicationName("In Phase");
    a.setApplicationDisplayName("In Phase");


   //srl->unauthorize();
   //srl->reactivateTrial();

//*
    if(!srl->isAuthorized())
    {
        dlg = new DialogAuth(NULL, srl);
        dlg->setWindowTitle("Authorisation");
        if(dlg->exec() == QDialog::Accepted)
        {
            delete dlg;
            m->setWindowTitle("In Phase");
            m->showMaximized();
            m->show();
            return a.exec();
        }
        else
            return 0;

    }
//*/

    m->setWindowTitle("In Phase");
    m->showMaximized();
    m->show();
    return a.exec();



}


