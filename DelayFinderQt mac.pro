#-------------------------------------------------
#
# Project created by QtCreator 2017-12-30T12:25:03
#
#-------------------------------------------------

QT       += core gui network
QT       += charts
QT       += multimedia

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = InPhase
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += \
        main.cpp \
    Sources/chartdrop.cpp \
    Sources/chartview.cpp \
    Sources/dialogauth.cpp \
    Sources/dialogparam.cpp \
    Sources/Freqseries.cpp \
    Sources/md5.cpp \
    Sources/serialprotect.cpp \
    Sources/smaart.cpp \
    Sources/timeseries.cpp \
    Sources/valueaxis.cpp \
    Sources/winmls.cpp \
    dialogabout.cpp \
    dialogmeasure.cpp \
    Sources/mainwindow.cpp \
    Sources/measurement.cpp \


HEADERS += \
    Headers/chartdrop.h \
    Headers/chartview.h \
    Headers/dialogauth.h \
    Headers/dialogparam.h \
    Headers/freqseries.h \
    Headers/mainwindow.h \
    Headers/md5.h \
    Headers/serialprotect.h \
    Headers/smaart.h \
    Headers/timeseries.h \
    Headers/valueaxis.h \
    Headers/winmls.h \
    dialogabout.h \
    dialogmeasure.h \
    Headers/measurement.h \

FORMS += \
    Forms/dialogauth.ui \
    Forms/dialogparam.ui \
    Forms/mainwindow.ui \
    dialogabout.ui \
    dialogmeasure.ui

win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../../../../../usr/local/lib/release/ -lsndfile
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../../../../../usr/local/lib/debug/ -lsndfile
else:unix: LIBS += -L$$PWD/../../../../../usr/local/lib/ -lsndfile

INCLUDEPATH += $$PWD/../../../../../usr/local/include
DEPENDPATH += $$PWD/../../../../../usr/local/include

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$PWD/../../../../../usr/local/lib/release/libsndfile.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$PWD/../../../../../usr/local/lib/debug/libsndfile.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$PWD/../../../../../usr/local/lib/release/sndfile.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$PWD/../../../../../usr/local/lib/debug/sndfile.lib
else:unix: PRE_TARGETDEPS += $$PWD/../../../../../usr/local/lib/libsndfile.a

win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../../../../../usr/local/lib/release/ -lfftw3
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../../../../../usr/local/lib/debug/ -lfftw3
else:unix: LIBS += -L$$PWD/../../../../../usr/local/lib/ -lfftw3

INCLUDEPATH += $$PWD/../../../../../usr/local/include
DEPENDPATH += $$PWD/../../../../../usr/local/include

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$PWD/../../../../../usr/local/lib/release/libfftw3.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$PWD/../../../../../usr/local/lib/debug/libfftw3.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$PWD/../../../../../usr/local/lib/release/fftw3.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$PWD/../../../../../usr/local/lib/debug/fftw3.lib
else:unix: PRE_TARGETDEPS += $$PWD/../../../../../usr/local/lib/libfftw3.a

win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../../../../../usr/local/lib/release/ -lrtaudio
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../../../../../usr/local/lib/debug/ -lrtaudio
else:unix: LIBS += -L$$PWD/../../../../../usr/local/lib/ -lrtaudio

INCLUDEPATH += $$PWD/../../../../../usr/local/include
DEPENDPATH += $$PWD/../../../../../usr/local/include

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$PWD/../../../../../usr/local/lib/release/librtaudio.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$PWD/../../../../../usr/local/lib/debug/librtaudio.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$PWD/../../../../../usr/local/lib/release/rtaudio.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$PWD/../../../../../usr/local/lib/debug/rtaudio.lib
else:unix: PRE_TARGETDEPS += $$PWD/../../../../../usr/local/lib/librtaudio.a
