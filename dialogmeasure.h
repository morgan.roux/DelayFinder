#ifndef DIALOGMEASURE_H
#define DIALOGMEASURE_H

#include "Headers/mainwindow.h"
#include "Headers/measurement.h"
#include "RtAudio.h"

#include <QDialog>
#include <complex>
#include <QString>
#include <sndfile.h>

#define I_complx std::complex<double>(0.0, 1.0)
//const double PI = std::acos(-1);

using namespace std;

namespace Ui {
class DialogMeasure;
}



class DialogMeasure : public QDialog
{

    // !!! DefaultInputName et defaultOutputName ne fonctionnent pas : il s'effacent lorsque l'on ferme
    //la fenêtre

    Q_OBJECT

public:
    explicit DialogMeasure(MainWindow *parent = 0);
    ~DialogMeasure();

    MainWindow *m_parent;
    void initUi();
    double **rec = NULL;          //Buffer for the record
    double *sweep = NULL;         // sweep for playing
    int unsigned recFrameIndex = 0;
    int unsigned playFrameIndex = 0;

    //Audio engine related
    RtAudio audioEngine;
    RtAudio::StreamParameters out_params;
    RtAudio::StreamParameters in_params;
    unsigned int bufferFrames = 512; //  number of samples per frame
    unsigned int fs = 48000;
    double *outMixer;
    unsigned int inputChannels[2];
    unsigned int outputChannels[2];

    int defaultInputItem = -1;
    int defaultOutputItem = -1;
    QString defaultInputName;               // !!!
    QString defaultOutputName;              // !!!

    //Getter & Setter
    double* getImpulse() const;
    unsigned int getSweepSize() const;
    unsigned int getFs() const;
    unsigned int getImpNumber() const;
    string getFileName() const;
    double *getSweep() const;
    unsigned int getRecSize() const;

    //Events
private slots:
    void on_button_Rec_clicked();
    void on_button_cancel_clicked();
    void on_input1Box_currentIndexChanged(int index);
    void on_output1Box_currentIndexChanged(int index);
    void on_testButton_toggled(bool checked);



private:
    //Ui and files

    Ui::DialogMeasure *ui;
    int impNumber = 1;
    string fileName;


    //sweep params
    unsigned int T, f1, f2;          // T : longueur du sweep en seconde
    double R;
    double t, tinv;
    unsigned int sweepSize;
    double *invsweep;

    //Windowing
    double* sweepWin;
    double* recWin;
    unsigned int fadeinLenght;          //in samples
    unsigned int fadeoutLenght;


    //For computing the impulse response
    double *impulseMeasr;
    double *impulseRef;
    unsigned int recSize;

    void startMeasure();
    void computeImpulseResponse();
};

#endif // DIALOGMEASURE_H
