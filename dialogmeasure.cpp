#include "dialogmeasure.h"
#include "ui_dialogmeasure.h"
#include "RtAudio.h"
#include "Headers/measurement.h"
#include <iostream>



//====================== Callback functions for audio engine =================//
int playsweep_rec( void *outputBuffer, void *inputBuffer, unsigned int nBufferFrames,
                   double streamTime, RtAudioStreamStatus status, void *userData )
{


    DialogMeasure *data = (DialogMeasure *)userData;
    unsigned int i, j;
    double *outprt = (double *) outputBuffer;
    double *rdptr = &data->sweep[data->playFrameIndex];

    double* inptr = (double*) inputBuffer;
    double** wptr = data->rec;

    //if ( status )
      //std::cout << "Stream underflow detected!" << std::endl;

    // Write interleaved audio data.
    for ( i=0; i<nBufferFrames; i++ )
    {
        //Play the sweep in the selected output channels
        if (data->playFrameIndex <= data->getSweepSize())
        {
            for ( j=0; j<data->out_params.nChannels; j++ )
                *outprt++ = data->outMixer[j] * (*rdptr);

            rdptr++;
            data->playFrameIndex++;
        }
        else
        {   //Sweep ended : silence
            for ( j=0; j<data->out_params.nChannels; j++ )
                *outprt++ = 0.0f;
        }

        //Record from the input
        if(data->recFrameIndex <= data->getRecSize())
        {
            for ( j=0; j<data->in_params.nChannels; j++ )
                  wptr[j][data->recFrameIndex] = *inptr++;

            data->recFrameIndex++;
        }
    }
    return 0;


}


int playsweeptest( void *outputBuffer, void *inputBuffer, unsigned int nBufferFrames,
                   double streamTime, RtAudioStreamStatus status, void *userData )
{

    DialogMeasure *data = (DialogMeasure *)userData;
    unsigned int i, j;
    double *outprt = (double *) outputBuffer;
    double* inptr = (double*) inputBuffer;
    double *rdptr = &data->sweep[data->playFrameIndex];

    //if ( status )
      //std::cout << "Stream underflow detected!" << std::endl;

    // Write interleaved audio data.
    for ( i=0; i<nBufferFrames; i++ )
    {
        //Play the sweeptest in the selected output channels
        if (data->playFrameIndex > data->getSweepSize())
        {   //Sweep ended : back to beginning
            data->playFrameIndex=0;
            rdptr = &data->sweep[data->playFrameIndex];
        }

        for ( j=0; j<data->out_params.nChannels; j++ )
        {
            *outprt++ = data->outMixer[j] * (*rdptr);
        }

        rdptr++;
        data->playFrameIndex++;
    }

    return 0;
}




// =================== DialogMeasure =============== //


DialogMeasure::DialogMeasure(MainWindow *parent) :
    QDialog(parent),
    ui(new Ui::DialogMeasure)
{
    m_parent = parent;

    //--- Initialize data ---//
    T=3; f1=20; f2=round(fs/2);              // T : longueur du sweep en seconde
    R = log(f2/f1);
    sweepSize = (int) round(T * fs);
    recSize = (unsigned int) round((T+1) * fs);        //On enregistre 1sec de plus

    fadeinLenght = floor(0.01f * fs);         //10 msec
    fadeoutLenght = floor(0.01f *fs);        //10 msec

    sweep = new double[sweepSize]();
    invsweep = new double[sweepSize]();
    impulseMeasr = new double[2*recSize - 1]();
    impulseRef = new double[2*recSize - 1]();
    sweepWin = new double[sweepSize]();
    recWin = new double[recSize]();

    //Windows : hanning
    {
    unsigned int i;
    for (i=0; i<fadeinLenght;i++)
        sweepWin[i] = 0.5f - cos(PI * (double)i / (double)fadeinLenght )/2.0f;
    for(; i<sweepSize - fadeoutLenght;i++)
        sweepWin[i] = 1;
    for(unsigned int j= 0; i< sweepSize; i++, j++)
        sweepWin[i] = cos(PI*(double)j / (double)fadeoutLenght)/2.0f + 0.5f;

    for (i=0; i<fadeinLenght;i++)
        recWin[i] = 0.5f - cos(PI * (double)i / (double)fadeinLenght )/2.0f;
    for(; i<recSize - fadeoutLenght;i++)
        recWin[i] = 1;
    for(unsigned int j= 0; i< recSize; i++, j++)
        recWin[i] = cos(PI*(double)j / (double)fadeoutLenght)/2.0f + 0.5f;
    }



    //Sweeps
    for (unsigned int i=0; i<sweepSize; i++)
    {
         t = (double)i / 48000.0f;
         sweep[i]= (double) sin((2*PI*f1*T/R)*(exp(t*R/T) - 1 ));
         sweep[i] *= sweepWin[i];                                       //Applying hann
    }

    //Inverse filter
    for (unsigned int i=0; i<sweepSize; i++)
    {
        t = (double)i / 48000.0f;
        invsweep[i]= sweep[sweepSize-1-i] / exp(t*R/T);
    }


    initUi();


}

void DialogMeasure::initUi()
{
    //---- Initialize ui ----//
    ui->setupUi(this);

    // Determine the number of devices available
    unsigned int devices = audioEngine.getDeviceCount();

    // Scan through devices for various capabilities and populate the boxes
    RtAudio::DeviceInfo info[devices];

    for ( unsigned int i=0; i<devices; i++ )
    {
        info[i] = audioEngine.getDeviceInfo( i );
        if (info[i].inputChannels != 0)
            ui->input1Box->addItem(QString::fromStdString(info[i].name), QVariant(i));
        if (info[i].outputChannels != 0)
            ui->output1Box->addItem(QString::fromStdString(info[i].name), QVariant(i));
    }


    //if first measurement since the software is open : select the default device
    if (defaultInputItem == -1)
    {
        defaultInputItem = ui->input1Box->findData(audioEngine.getDefaultInputDevice());
        defaultOutputItem = ui->output1Box->findData(audioEngine.getDefaultOutputDevice());

        defaultInputName = ui->input1Box->itemText(defaultInputItem);
        defaultOutputName = ui->output1Box->itemText(defaultOutputItem);
    }
    //else : select the device previously selected          !!!!
    else
    {
        defaultInputItem = ui->input1Box->findText(defaultInputName);
        defaultOutputItem = ui->output1Box->findText(defaultOutputName);
    }

    ui->input1Box->setCurrentIndex(defaultInputItem);
    ui->output1Box->setCurrentIndex(defaultOutputItem);

    //if several channels : setting two different channels by default
    if (ui->inChannel1Box->count() > 1)
    {
        ui->inChannel1Box->setCurrentIndex(0);
        ui->inChannel2Box->setCurrentIndex(1);
    }
    if (ui->outChannel1Box->count() > 1)
    {
        ui->outChannel1Box->setCurrentIndex(0);
        ui->outChannel2Box->setCurrentIndex(1);
    }
}



void DialogMeasure::startMeasure()
{

    ui->button_Rec->setDisabled(true);

    //Retrieving the input and output channels
    inputChannels[0] = ui->inChannel1Box->currentData().toInt();
    inputChannels[1] = ui->inChannel2Box->currentData().toInt();
    outputChannels[0] = ui->outChannel1Box->currentData().toInt();
    outputChannels[1] = ui->outChannel2Box->currentData().toInt();


    //Setting params
    out_params.deviceId = ui->output1Box->currentData().toInt();
    out_params.nChannels = audioEngine.getDeviceInfo(out_params.deviceId).outputChannels;
    out_params.firstChannel = 0;

    in_params.deviceId = ui->input1Box->currentData().toInt();
    in_params.nChannels = audioEngine.getDeviceInfo(in_params.deviceId).inputChannels;
    in_params.firstChannel = 0;


    //Allocation de mémoire pour l'enregistrement
    //On enregistre pendant T+1sec pour les cas où le système
    //est très retardé.

    rec = new double *[in_params.nChannels];                  //row : input channel number
    for (unsigned int i=0; i<in_params.nChannels;i++)
        rec[i] = new double[recSize]();                      //column : time

    //Setting output mixer
    outMixer = new double[out_params.nChannels]();          //All gain at -inf
    outMixer[outputChannels[0]] = 1.0f;                     //0dB
    outMixer[outputChannels[1]] = 1.0f;                     //0dB


    //Setting duplex stream
    playFrameIndex = 0;
    recFrameIndex = 0;
    try
    {
      audioEngine.openStream( &out_params, &in_params, RTAUDIO_FLOAT64,
                      fs, &bufferFrames, &playsweep_rec, (void *)this );
      audioEngine.startStream();
    }

    catch ( RtAudioError& e )
    {
      e.printMessage();
      exit( 0 );
    }

    //On enregistre pendant T+1 sec
    QTime dieTime= QTime::currentTime().addSecs(T+1);
    while (QTime::currentTime() < dieTime)
        QCoreApplication::processEvents(QEventLoop::AllEvents, 100);

    audioEngine.stopStream();
    audioEngine.closeStream();

    computeImpulseResponse();


    if (ui->radio_imp1->isChecked())
        impNumber=1;
    else
        impNumber=2;


    this->accept();

}



void DialogMeasure::computeImpulseResponse()
{

    SNDFILE *file;
    SF_INFO info;
    sf_count_t idx;

    double* recMeasr;
    double* recRef;
    double* impulseFinal;

    recMeasr = new double[recSize]();
    recRef = new double[recSize]();
    impulseFinal = new double[2*recSize-1]();

    //Transfer record samples and windowing
    for (unsigned int i=0; i< recSize; i++)
    {
        recMeasr[i] = rec[inputChannels[0]][i] * recWin[i];
        recRef[i] = rec[inputChannels[1]][i] * recWin[i];

    }

    //Computing Impulse response
    conv(recMeasr,invsweep,impulseMeasr,recSize,sweepSize,1);
    conv(recRef,invsweep,impulseRef,recSize,sweepSize,1);

    //Lors de la convolution dans la méthode de Angelo Farina, l'impulse est décalée de sweepSize
    //Donc on enlève la première partie.
     impulseMeasr += (sweepSize-1);
     impulseRef += (sweepSize-1);


    //Deconvolution (on ne décale pas ici, le décalage est uniquement à faire pour farina)
    if(ui->checkBox->isChecked())
        conv(impulseMeasr, impulseRef, impulseFinal,recSize, recSize, -1);
    else //not using reference signal. storing the measure without deconvolving
        memcpy(impulseFinal,impulseMeasr,sizeof(double)*recSize);

    //Ecriture dans un fichier
    info.channels = 1;
    info.format = SF_FORMAT_WAV | SF_FORMAT_PCM_16;
    info.samplerate = fs;

    fileName = (QFileDialog::getSaveFileName(this, tr("Save impulse to file"),tr("/Users"),tr("*.wav"),NULL,NULL)).toStdString();

    if (fileName == "")
            return;

    //Test de l'extension
    int l = fileName.length();
    string ext = fileName.substr(l-4,4);
    if (ext.compare(".wav") != 0 )
        fileName.append(".wav");

    //Si le fichier existe, on le supprime pour pouvoir le mettre à jour
    FILE* fl;
    if ((fl=fopen(fileName.c_str(),"r")) != NULL)
    {
        fclose(fl);
        remove(fileName.c_str());
    }


    file = sf_open(fileName.c_str(),SFM_RDWR, &info);
    idx = sf_write_double(file,impulseFinal,sweepSize);
    sf_close(file);

    delete recMeasr;
    delete recRef;
    delete impulseFinal;


    return;

}



DialogMeasure::~DialogMeasure()
{

    delete sweep;
    delete invsweep;

    delete outMixer;

    for (unsigned int i=0; i<in_params.nChannels;i++)
        delete rec[i];
    delete rec;
    delete ui;
    delete sweepWin;
    delete recWin;
    //delete impulseMeasr;
    //delete impulseRef;


}


// ================= Events ========================== //

void DialogMeasure::on_button_Rec_clicked()
{

    startMeasure();

}

void DialogMeasure::on_button_cancel_clicked()
{
    this->reject();
}



void DialogMeasure::on_input1Box_currentIndexChanged(int index)
{
    //populate the channel boxes
    int device = ui->input1Box->itemData(index).toInt();
    int nChann = audioEngine.getDeviceInfo(device).inputChannels;

    ui->inChannel1Box->clear();
    ui->inChannel2Box->clear();

    for(int i = 0;i<nChann; i++)
    {

        ui->inChannel1Box->addItem(tr("Input %1").arg(i+1),QVariant(i));
        ui->inChannel2Box->addItem(tr("Input %1").arg(i+1),QVariant(i));
    }

    //update the default input device
    defaultInputName = ui->input1Box->itemText(index);

    //if several channels : setting two different channels by default
    if (nChann > 1)
    {
        ui->inChannel1Box->setCurrentIndex(0);
        ui->inChannel2Box->setCurrentIndex(1);
    }
}

void DialogMeasure::on_output1Box_currentIndexChanged(int index)
{   //populate the channel boxes

    int device = ui->output1Box->itemData(index).toInt();
    int nChann = audioEngine.getDeviceInfo(device).outputChannels;

    ui->outChannel1Box->clear();
    ui->outChannel2Box->clear();

    for(int i = 0;i<nChann; i++)
    {
        ui->outChannel1Box->addItem(tr("Output %1").arg(i+1),QVariant(i));
        ui->outChannel2Box->addItem(tr("Output %1").arg(i+1),QVariant(i));
    }

    //update the default output device
    defaultOutputName = ui->output1Box->itemText(index);

    //if several channels : setting two different channels by default
    if (nChann > 1)
    {
        ui->outChannel1Box->setCurrentIndex(0);
        ui->outChannel2Box->setCurrentIndex(1);
    }
}

void DialogMeasure::on_testButton_toggled(bool checked)
{
    if (checked)
    {// Lecture d'un sweep
        //Retrieving the input and output channels
        inputChannels[0] = ui->inChannel1Box->currentData().toInt();
        inputChannels[1] = ui->inChannel1Box->currentData().toInt();
        outputChannels[0] = ui->outChannel1Box->currentData().toInt();
        outputChannels[1] = ui->outChannel2Box->currentData().toInt();


        //Setting params
        in_params.deviceId = ui->input1Box->currentData().toInt();
        in_params.nChannels = audioEngine.getDeviceInfo(in_params.deviceId).inputChannels;
        in_params.firstChannel = 0;
        out_params.deviceId = ui->output1Box->currentData().toInt();
        out_params.nChannels = audioEngine.getDeviceInfo(out_params.deviceId).outputChannels;
        out_params.firstChannel = 0;

        //Setting output mixer
        outMixer = new double[out_params.nChannels]();
        outMixer[outputChannels[0]] = 1.0f;
        outMixer[outputChannels[1]] = 1.0f;


        //Duplex stream
        playFrameIndex = 0;
        try
        {
          audioEngine.openStream( &out_params, &in_params, RTAUDIO_FLOAT64,
                          fs, &bufferFrames, &playsweeptest, (void *)this );
          audioEngine.startStream();
        }

        catch ( RtAudioError& e )
        {
          e.printMessage();
          exit( 0 );
        }


    }
    else
    {//Stoppe la lecture
        audioEngine.stopStream();
        audioEngine.closeStream();

    }

}

unsigned int DialogMeasure::getRecSize() const
{
    return recSize;
}






double *DialogMeasure::getSweep() const
{
    return sweep;
}


string DialogMeasure::getFileName() const
{
    return fileName;
}

unsigned int DialogMeasure::getImpNumber() const
{
    return impNumber;
}

unsigned int DialogMeasure::getFs() const
{
    return fs;
}

unsigned int DialogMeasure::getSweepSize() const
{
    return sweepSize;
}

double *DialogMeasure::getImpulse() const
{
    return impulseMeasr;
}








/*
 int playsweep( void *outputBuffer, void *inputBuffer, unsigned int nBufferFrames,
         double streamTime, RtAudioStreamStatus status, void *userData )
{

  DialogMeasure *data = (DialogMeasure *)userData;
  unsigned int i, j;
  double *outprt = (double *) outputBuffer;
  double *rdptr = &data->sweep[data->playFrameIndex];

  if ( status )
    std::cout << "Stream underflow detected!" << std::endl;

  // Write interleaved audio data.
  for ( i=0; i<nBufferFrames; i++ )
  {
        outprt[0] = *rdptr;         //left
        outprt[1] = *rdptr;         //right
        outprt+=2;
        rdptr++;


    if (data->playFrameIndex++ == data->getSweepSize())
        //Sweep finished
        return 1;
  }

  return 0;
}


int record( void *outputBuffer, void *inputBuffer, unsigned int nBufferFrames,
         double streamTime, RtAudioStreamStatus status, void *userData )
{

   unsigned int i,j;

   DialogMeasure *data = (DialogMeasure *)userData;
   double* inptr = (double*) inputBuffer;
   double** wptr = data->rec;

   if ( status )
     std::cout << "Stream overflow detected!" << std::endl;

   for (i=0;i<nBufferFrames;i++)
   {
       for ( j=0; j<data->in_params.nChannels; j++ )
             wptr[data->recFrameIndex][j] = *inptr++;

       data->recFrameIndex++;
   }

   return 0;
}

int playrec( void *outputBuffer, void *inputBuffer, unsigned int nBufferFrames,
         double streamTime, RtAudioStreamStatus status, void *userData )
{

  DialogMeasure *data = (DialogMeasure *)userData;
  unsigned int i,j;
  double *outprt = (double *) outputBuffer;
  double *rdptr = &data->rec[data->playFrameIndex*2];

  if ( status )
    std::cout << "Stream underflow detected!" << std::endl;

  // Write interleaved audio data.
  for ( i=0; i<nBufferFrames; i++ )
  {
        outprt[0] = rdptr[0];         //left
        outprt[1] = rdptr[1];         //right
        outprt+=2;
        rdptr+=2;


    if (data->playFrameIndex++ == data->recFrameIndex)
        //Sweep finished
        return 1;
  }

  return 0;
}
//*/

