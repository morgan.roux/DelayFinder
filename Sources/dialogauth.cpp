#include "Headers/dialogauth.h"
#include "ui_dialogauth.h"

DialogAuth::DialogAuth(QWidget *parent, SerialProtect* serial) :
    QDialog(parent),
    ui(new Ui::DialogAuth)
{
    srl = serial;

    ui->setupUi(this);    
    connect(ui->buttonBox, &QDialogButtonBox::accepted, this, &DialogAuth::on_buttonOk_click);
    connect(ui->buttonTrial, &QPushButton::clicked, this, &DialogAuth::on_buttonTrial_click);

    ui->lineSerial->setText( QString(srl->getSerial().c_str()) );

    //if(srl->checkTrialPeriod())
    if(false)
    {
        //Trial period ok
        ui->buttonTrial->setEnabled(true);
        ui->label_4->setVisible(false);
    }
    else
    {
        //Trial Period expired
        ui->buttonTrial->setEnabled(false);
        ui->label_4->setVisible(true);
    }
}

DialogAuth::~DialogAuth()
{
    delete ui;
}

void DialogAuth::on_buttonOk_click()
{
    //Check key
    if( srl->verifyKey(srl->getSerial(), ui->lineKey->text().toStdString()) )
    {
        srl->writeKey(ui->lineKey->text().toStdString());
        QMessageBox msgBox;
        msgBox.setText("Software Authorized ! Enjoy !");
        msgBox.exec();
        accept();
        return;
    }


    QMessageBox msgBox;
    msgBox.setText("Wrong Key");
    msgBox.exec();

    reject();
    return;

}

void DialogAuth::on_buttonTrial_click()
{
    if(ui->buttonTrial->isEnabled())
        accept();
}
