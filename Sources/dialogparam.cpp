#include "Headers/dialogparam.h"
#include "ui_dialogparam.h"

DialogParam::DialogParam(MainWindow *parent) :
    QDialog(parent),
    ui(new Ui::DialogParam)
{
    m_parent = parent;


    ui->setupUi(this);
    ui->comboBox_phase->setCurrentIndex(m_parent->phase());
    ui->lineEdit_fmin->setText(QString::number(m_parent->fmin()));
    ui->lineEdit_fmax->setText(QString::number(m_parent->fmax()));
    ui->lineEdit_mmin->setText(QString::number(m_parent->mmin()));
    ui->lineEdit_mmax->setText(QString::number(m_parent->mmax()));

   connect(ui->buttonBox,&QDialogButtonBox::accepted,this,&DialogParam::updateparams);
   // connect(DialogParam,&DialogParam::paramUpdate,);
}

DialogParam::~DialogParam()
{
    delete ui;
}

void DialogParam::updateparams()
{
    m_parent->setPhase(ui->comboBox_phase->currentIndex());
    m_parent->setFmin(ui->lineEdit_fmin->text().toInt());
    m_parent->setFmax(ui->lineEdit_fmax->text().toInt());
    m_parent->setMmin(ui->lineEdit_mmin->text().toInt());
    m_parent->setMmax(ui->lineEdit_mmax->text().toInt());

    accept();
}


