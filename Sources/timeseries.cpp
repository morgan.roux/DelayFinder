#include "Headers/timeseries.h"

TimeSeries::TimeSeries() : QLineSeries()
{

}

TimeSeries::TimeSeries(double *x, double *y, int n) : QLineSeries()
{
    this->newSeries(x,y,n);
}

void TimeSeries::newSeries(double *x, double *y, int n)
{
    for (int i=0;i<n;i++)
        this->append(x[i], y[i]);
}

