#include "Headers/serialprotect.h"

SerialProtect::SerialProtect()
{
    createSerial();
}

bool SerialProtect::createSerial()
{
    QString macAddr;

    macAddr = getMacAddress();
    if (macAddr.isEmpty())
        return false;

    Serial = md5(macAddr.toStdString());

    return true;

}

bool SerialProtect::isAuthorized()
{
    QSettings settings("Wevo Audio", "In Phase");

    settings.beginGroup("Auth");

    if (verifyKey(Serial, settings.value("ActKey").toString().toStdString()) )
    {
        settings.endGroup();
        return true;
    }
    else
    {
        settings.endGroup();

        return false;
    }

}

bool SerialProtect::verifyKey(string sn, string key)
{
    sn.append("InPhaseMorganRoux");
    string test = md5(sn);

    if(key.compare(md5(sn)) == 0)
        return true;

    return false;
}

QString SerialProtect::getMacAddress()
{
    QString str;


    foreach(QNetworkInterface netInterface, QNetworkInterface::allInterfaces())
    {
        // Return only the first non-loopback MAC Address
        str = netInterface.humanReadableName();

        if (!(netInterface.flags() & QNetworkInterface::IsLoopBack))
        {
            str = netInterface.hardwareAddress();
            if(!str.isEmpty())
                return str;
        }
    }
    return QString();

}

void SerialProtect::writeKey(string strg)
{
    //on passe un const char* à QVariant, mais c'est un QString qui est stockée

    QSettings settings("Wevo Audio", "In Phase");
    settings.beginGroup("Auth");
    settings.setValue("ActKey", QVariant(strg.c_str()));
    settings.endGroup();

}

void SerialProtect::unauthorize()
{
    QSettings settings("Wevo Audio", "In Phase");
    settings.beginGroup("Auth");
    settings.remove("ActKey");
    settings.endGroup();
}

void SerialProtect::reactivateTrial()
{
    QSettings settings("Wevo Audio", "In Phase");
    settings.beginGroup("Auth");
    settings.remove("Trial");
    settings.endGroup();
}

bool SerialProtect::checkTrialPeriod()
{
    //Test : Key exist ?
    QDate currentDate = QDate::currentDate();
    QVariant trialDate;

    QSettings settings("Wevo Audio", "In Phase");
    settings.beginGroup("Auth");


    if((trialDate = settings.value("Trial")).isNull())
    {
        //1st use : store the date of the 1st use
        settings.setValue("Trial", QVariant(currentDate));
        settings.endGroup();
        return true;
    }
    else
    {
        //Test if the trial expired : 1 month

        if(currentDate.daysTo(trialDate.toDate().addDays(TrialPeriod)) <= 0)
        {   //expired
            settings.endGroup();
            return false;
        }
        else
        {   //still ok
            settings.endGroup();
            return true;
        }
    }

}

string SerialProtect::getSerial() const
{
    return Serial;
}

