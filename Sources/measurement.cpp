#include "Headers/measurement.h"
using namespace std;



Impulse::Impulse(double* imp, unsigned int fs, unsigned int samples, unsigned int winlen = 0)
{
    m_Fs = fs;
    m_samples = samples;
    m_nfft = floor(m_samples/2) +1;
    m_winlen = winlen;

    m_x = new double[m_samples]();
    m_y = new double[m_samples]();

    for (long l =0; l<m_samples; l++)
        m_x[l]=l;

    if (imp != NULL)
        memcpy(m_y,imp,sizeof(double)*m_samples);
}



Impulse::Impulse(std::string filenm)
{


    FILE* fid = NULL;
    WinMLS Winmls;
    Smaart Smrt;

    //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    // OPEN FILE
    //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    fid = fopen(filenm.c_str(), "rb");

    if (fid == NULL )
    {
        QMessageBox msgBox;
        msgBox.setText("Error. Could not open a file with the the specified filename.");
        msgBox.exec();
        throw 1;

        return;
    }


    //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    //% FIND EXTENSION
    //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    int l = filenm.length();
    /*
    if (l<3) // check that length of string is larger than three.
    {
        QMessageBox msgBox;
        msgBox.setText("Error. Wrong extension - Either .WMB or .WMT must be used as file extensions");
        msgBox.exec();
        fclose(fid);
        return false ;
    }
    //*/

    string ext = filenm.substr(l-3,3);


    //%%%%%%%%%%%%%%%%%%%
    //% WMB : WinMLS
    //%%%%%%%%%%%%%%%%%%%
    if (ext.compare("wmb") == 0 )
    {
        if(Winmls.loadImp(fid))
        {
            m_Fs = Winmls.getFs();
            if((m_samples = Winmls.getLength()) > SAMPLES_MAX)
                m_samples = SAMPLES_MAX;

            m_nfft = floor(m_samples/2) +1;
            m_x = new double[m_samples];
            m_y = new double[m_samples];

            for (long l =0; l<m_samples; l++)
            {
                m_x[l]=l;
                m_y[l]=(double)Winmls.H()[l];
            }
        }
    }

    //%%%%%%%%%%%%%%%%%%%
    //% TRF : Smaart
    //%%%%%%%%%%%%%%%%%%%
    else if (ext.compare("trf") == 0 )
    {
        if (Smrt.loadImp(fid))
        {
            m_Fs = Smrt.SR();
            if((m_samples = Smrt.Bins()) > SAMPLES_MAX)
                m_samples = SAMPLES_MAX;

            m_nfft = floor(m_samples/2) +1;
            m_x = new double[m_samples];
            m_y = new double[m_samples];

            for (long l =0; l<m_samples; l++)
            {
                m_x[l]=l;
                m_y[l]=(double)Smrt.getLIR()[l];
            }
        }
    }

    //%%%%%%%%%%%%%%%%%%%
    //% WAV
    //%%%%%%%%%%%%%%%%%%%
    else if (ext.compare("wav") == 0 )
    {
        SNDFILE *file;
        SF_INFO info;
        sf_count_t idx;

        file = sf_open(filenm.c_str(),SFM_READ, &info);
        m_Fs = info.samplerate;

        // m_filename = filename

        if((m_samples = info.frames*info.channels) > SAMPLES_MAX)
           m_samples = SAMPLES_MAX;

        m_nfft = floor(m_samples/2) +1;
        m_x = new double[m_samples];
        m_y = new double[m_samples];

        idx = sf_read_double(file,m_y,m_samples);
        sf_close(file);

        for (long l =0; l<m_samples; l++)
            m_x[l]=l;
    }

    //%%%%%%%%%%%%%%%%%%%
    //% ERROR
    //%%%%%%%%%%%%%%%%%%%
    else
    {
        QMessageBox msgBox;
        msgBox.setText("Error. Wrong extension - Either .wmb / .trf / .wav must be used as file extensions");
        msgBox.exec();

    }
    fclose(fid);

    return;

}

/*=============================================================*/

Impulse::Impulse(const Impulse &imp)
    : m_Fs(imp.m_Fs),
      m_samples(imp.m_samples),      
      m_nfft(imp.m_nfft),
      m_winlen(imp.m_winlen)
{
    m_x = new double[m_samples];
    m_y = new double[m_samples];

    memcpy(m_x,imp.m_x,sizeof(double)*m_samples);
    memcpy(m_y,imp.m_y,sizeof(double)*m_samples);


    // #############
   // m_filename = new char(*(imp.m_filename));


}

Impulse::~Impulse()
{
    if (m_x != NULL)
        delete m_x;
    if (m_y != NULL)
        delete m_y;

    //free(m_x);
    //free(m_y);

}

void Impulse::timeShift(long delay)
{//delay en samples

    double* temp;


    if (delay>0)
    {
        temp = (double*)malloc(sizeof(double) * m_samples);
        memset(temp,0, sizeof(double) * delay);
        memcpy(temp + delay,m_y,sizeof(double)*(m_samples - delay));
        memcpy(m_y,temp,sizeof(double)*(m_samples));
        free(temp);
    }

    if(delay<0)
    {
        delay = -delay;

        temp = (double*)malloc(sizeof(double) * m_samples);
        memcpy(temp,m_y + delay,sizeof(double)*(m_samples - delay));
        memcpy(m_y,temp,sizeof(double)*(m_samples));
        free(temp);
    }


}

void Impulse::normalize()
{
    Peak pk = findAbsolutePeak(m_y,m_samples);
    double max = abs(pk.max);

    if (max != 0)
    {
        for(int i=0; i< m_samples; i++)
            m_y[i] = m_y[i] / max;
    }
}

void Impulse::antiphase()
{
    for (int i=0;i<m_samples;i++)
        m_y[i] = -m_y[i];
}

void Impulse::clear()
{

    delete m_y;
    m_y = new double[m_samples]();
}

//========================
long Impulse::samples() const
{
    return m_samples;
}

void Impulse::setSamples(long samples)
{
    m_samples = samples;
}

int Impulse::winlen() const
{
    return m_winlen;
}

void Impulse::setWinlen(int winlen)
{
    m_winlen = winlen;
}

string Impulse::filename() const
{
    return m_filename;
}


double *Impulse::getX() const
{
    return m_x;
}

void Impulse::setX(double *value)
{
    m_x = value;
}

double *Impulse::getY() const
{
    return m_y;
}

void Impulse::setY(double *value)
{
    m_y = value;
}

int Impulse::getFs() const
{
    return m_Fs;
}

void Impulse::setFs(int Fs)
{
    m_Fs = Fs;
}



//================= TransFct ========================

TF::TF(unsigned short Fs, unsigned short samples)
    : m_Fs(Fs),
      m_samples(samples)
{
    m_nfft = floor(m_samples/2) +1;
    m_Fprec = m_Fs / m_samples;
    m_f = new double[m_nfft];
    m_mag = new double[m_nfft];
    m_phs = new double[m_nfft];
    m_cpx = new std::complex<double>[m_nfft];

    for(long l=0;l<m_nfft;l++)
        m_f[l] = l*m_Fprec;


}

TF::TF(TF const& tf)
{
    m_Fs = tf.m_Fs;
    m_Fprec = tf.m_Fprec;
    m_samples = tf.m_samples;
    m_nfft = tf.m_nfft;

    m_f = new double[m_nfft];
    m_mag = new double[m_nfft];
    m_phs = new double[m_nfft];
    m_cpx = new std::complex<double>[m_nfft];

    memcpy(m_f,tf.m_f,sizeof(double) * m_nfft);
    memcpy(m_mag,tf.m_mag,sizeof(double) * m_nfft);
    memcpy(m_phs,tf.m_phs,sizeof(double) * m_nfft);
    memcpy(m_cpx,tf.m_cpx,sizeof(std::complex<double>) * m_nfft);


    /*
    m_f = new double(*(tf.m_f));
    m_mag = new double(*(tf.m_mag));
    m_phs = new double(*(tf.m_phs));
    m_cpx = new std::complex<double>(*(tf.m_cpx));
    */
}

TF::~TF()
{
    free(m_f);
    free(m_mag);
    free(m_phs);
    free(m_cpx);

}

void TF::timeShift(int delay)
{   //delay en samples
    //On mutiplie par exp(2π.f.delay)
    for(int i=0;i<m_nfft;i++)
    {
        m_cpx[i] = m_cpx[i] * std::exp(-2*PI*I_complx * std::complex<double>(m_f[i] * (double)delay / (double) m_Fs));
    }

    this->cpx2polar();


}

void TF::cpx2polar()
{
    for (long l=0; l<m_nfft; l++)
    {
        m_mag[l] = 20*log10(std::abs(m_cpx[l]));
        m_phs[l] = std::arg(m_cpx[l]);
    }

}

void TF::sum(TF *tf)
{
    if(m_Fs==tf->Fs() && m_Fprec==tf->Fprec()
      && m_samples==tf->samples() && m_nfft==tf->nfft())
    {
        for (long l=0;l<m_nfft;l++)
            m_cpx[l] += (tf->cpx())[l];

        this->cpx2polar();
    }

}

void TF::div(TF *tf)
{
    if(m_Fs==tf->Fs() && m_Fprec==tf->Fprec()
      && m_samples==tf->samples() && m_nfft==tf->nfft())
    {
        for (long l=0;l<m_nfft;l++)
            m_cpx[l] /= (tf->cpx())[l];

        this->cpx2polar();
    }

}


//===========================
std::complex<double> *TF::cpx() const
{
    return m_cpx;
}

void TF::setCpx(std::complex<double> *cpx)
{
    m_cpx = cpx;
}

double *TF::f() const
{
    return m_f;
}

void TF::setF(double *f)
{
    m_f = f;
}

double *TF::mag() const
{
    return m_mag;
}

void TF::setMag(double *mag)
{
    m_mag = mag;
}

double *TF::phs() const
{
    return m_phs;
}

void TF::setPhs(double *phs)
{
    m_phs = phs;
}

long TF::nfft() const
{
    return m_nfft;
}

void TF::setNfft(long nfft)
{
    m_nfft = nfft;
}

double TF::Fprec() const
{
    return m_Fprec;
}

void TF::setFprec(double Fprec)
{
    m_Fprec = Fprec;
}
int TF::Fs() const
{
    return m_Fs;
}

void TF::setFs(int Fs)
{
    m_Fs = Fs;
}


long TF::samples() const
{
    return m_samples;
}

void TF::setSamples(long samples)
{
    m_samples = samples;
}



/*===================================================*/

void fft(Impulse *imp, TF *tf)
{
    //FFT - no scaling
    double *in;
    fftw_complex *out;
    fftw_plan p;

    if(imp->m_samples!= tf->samples())
        return;

    in = (double*) fftw_malloc(sizeof(double) * imp->m_samples);
    out = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * imp->m_samples);
    p = fftw_plan_dft_r2c_1d(imp->m_samples, in, out, FFTW_PRESERVE_INPUT && FFTW_MEASURE);

    memcpy(in, imp->m_y, sizeof(double)*imp->m_samples);
    fftw_execute(p);
    memcpy(tf->cpx(), out, sizeof(fftw_complex) * imp->m_nfft);

    tf->cpx2polar();

    fftw_destroy_plan(p);
    fftw_free(in);
    fftw_free(out);

    return;
}

void ifft(Impulse *imp, TF *tf)
{

    fftw_complex *in;
    double *out;
    fftw_plan p;
    int n;

    if(imp->m_samples!= tf->samples())
        return;

    n = imp->m_samples;

    out = (double*) fftw_malloc(sizeof(double) * n);
    in = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * n);
    p = fftw_plan_dft_c2r_1d(n, in, out, FFTW_PRESERVE_INPUT && FFTW_MEASURE);


    memcpy(in, tf->cpx(), sizeof(fftw_complex) * imp->m_nfft);
    fftw_execute(p);
    memcpy(imp->m_y, out, sizeof(double)*imp->m_samples);

    fftw_destroy_plan(p);
    fftw_free(in);
    fftw_free(out);

    return;


}

void xCorr(double* signala, double* signalb, double* result, int N)
{
    //size signala = size signalb = N
    //size result = 2*N-1

    double * in_a = (double *) fftw_malloc(sizeof(double) * (2 * N - 1));
    double* in_b = (double *) fftw_malloc(sizeof(double) * (2 * N - 1));
    complex<double> * out_a = (complex<double> *) fftw_malloc(sizeof(complex<double>) * (2 * N - 1));
    complex<double> * out_b = (complex<double> *) fftw_malloc(sizeof(complex<double>) * (2 * N - 1));
    complex<double> * out = (complex<double> *) fftw_malloc(sizeof(complex<double>) * (2 * N - 1));

    complex<double> scale1 = 1.0/(2 * N -1); //scale for fft->ifft
    double scalea=0;
    double scaleb=0;

    fftw_plan pa = fftw_plan_dft_r2c_1d(2*N-1, in_a,reinterpret_cast<fftw_complex*>(out_a),FFTW_ESTIMATE); // fftw_plan_dft_1d(2 * N - 1, in_a, out_a, FFTW_FORWARD, FFTW_ESTIMATE);
    fftw_plan pb = fftw_plan_dft_r2c_1d(2*N-1, in_b,reinterpret_cast<fftw_complex*>(out_b),FFTW_ESTIMATE); //fftw_plan_dft_1d(2 * N - 1, in_b, outb, FFTW_FORWARD, FFTW_ESTIMATE);
    fftw_plan px = fftw_plan_dft_c2r_1d(2*N-1, reinterpret_cast<fftw_complex*>(out),result,FFTW_ESTIMATE); // fftw_plan_dft_1d(2 * N - 1, out, result, FFTW_BACKWARD, FFTW_ESTIMATE);

    //zeropadding to avoid circular cross correlation and center the result
    memset (in_a, 0, sizeof(double) * (N - 1));
    memcpy (in_a + (N - 1), signala, sizeof(double) * N);
    memcpy (in_b, signalb, sizeof(double) * N);
    memset (in_b + N, 0, sizeof(double) * (N - 1));

    //FFTs
    fftw_execute(pa);
    fftw_execute(pb);

    //calcul de la norme de in_a et in_b
    for(int i=0;i<2*N-1;i++)
    {
        scalea+=in_a[i]*in_a[i];
        scaleb+=in_b[i]*in_b[i];
    }


    //Cross correllation
    for (int i = 0; i < 2 * N - 1; i++)
    {
        out[i] = out_a[i] * conj(out_b[i]);                                 //FFT(a) * conj( FFT(b) )
        out[i] = out[i] * scale1 / complex<double>(sqrt(scalea*scaleb));    //Normalization
    }

    //iFFT
    fftw_execute(px);

    fftw_destroy_plan(pa);
    fftw_destroy_plan(pb);
    fftw_destroy_plan(px);

    fftw_free(in_a);
    fftw_free(in_b);
    fftw_free(out);
    fftw_free(out_a);
    fftw_free(out_b);

    fftw_cleanup();

    return;
}


//------------------------//

void conv(double* signala, double* signalb, double* result,
          unsigned int lenghta, unsigned int lenghtb, int action)
{
    //size result = 2*N-1, but only [O -> N-1] is revelant
    //result have to be initialized already with a size of (2*N -1)

    unsigned int N = max(lenghta,lenghtb);

    double* in_a = (double *) fftw_malloc(sizeof(double) * (2 * N - 1));
    double* in_b = (double *) fftw_malloc(sizeof(double) * (2 * N - 1));
    complex<double> * out_a = (complex<double> *) fftw_malloc(sizeof(complex<double>) * (2 * N - 1));
    complex<double> * out_b = (complex<double> *) fftw_malloc(sizeof(complex<double>) * (2 * N - 1));
    complex<double> * out = (complex<double> *) fftw_malloc(sizeof(complex<double>) * (2 * N - 1));

    complex<double> scale1 = 1.0/(2 * N -1); //scale for fft->ifft
    double scalea=0;
    double scaleb=0;

    fftw_plan pa = fftw_plan_dft_r2c_1d(2*N-1, in_a,reinterpret_cast<fftw_complex*>(out_a),FFTW_ESTIMATE); // fftw_plan_dft_1d(2 * N - 1, in_a, out_a, FFTW_FORWARD, FFTW_ESTIMATE);
    fftw_plan pb = fftw_plan_dft_r2c_1d(2*N-1, in_b,reinterpret_cast<fftw_complex*>(out_b),FFTW_ESTIMATE); //fftw_plan_dft_1d(2 * N - 1, in_b, outb, FFTW_FORWARD, FFTW_ESTIMATE);
    fftw_plan px = fftw_plan_dft_c2r_1d(2*N-1, reinterpret_cast<fftw_complex*>(out),result,FFTW_ESTIMATE); // fftw_plan_dft_1d(2 * N - 1, out, result, FFTW_BACKWARD, FFTW_ESTIMATE);

    //zeropadding to avoid circular convolution
    memset (in_a, 0, sizeof(double) * (2*N - 1));
    memcpy (in_a, signala, sizeof(double) * lenghta);
    memset (in_b, 0, sizeof(double) * (2*N - 1));
    memcpy (in_b, signalb, sizeof(double) * lenghtb);

    //FFTs
    fftw_execute(pa);
    fftw_execute(pb);

    //calcul de la norme de in_a et in_b
    for(int i=0;i<2*N-1;i++)
    {
        scalea+=in_a[i]*in_a[i];
        scaleb+=in_b[i]*in_b[i];
    }


    if(action == 1 )
    {
        //Convolution
        for (int i = 0; i < 2 * N - 1; i++)
        {
            out[i] = out_a[i] * out_b[i];                                   //FFT(a) *  FFT(b)
            out[i] = out[i] * scale1;                                       //Normalization FFT
        //    out[i] = out[i] / complex<double>(sqrt(scalea*scaleb)) ;       //Normalization to 1
        }
    }

    else if(action == -1 )
    {
        //Deconvolution
        for (int i = 0; i < 2 * N - 1; i++)
        {
            out[i] = out_a[i] / out_b[i];                                       //FFT(a) /  FFT(b)
            out[i] = out[i] * scale1;                                       //Normalization FFT
     //       out[i] = out[i] / complex<double>(sqrt(scalea/scaleb)) ;       //Normalization to 1
        }
    }


    //iFFT
    fftw_execute(px);

    //Clean
    fftw_destroy_plan(pa);
    fftw_destroy_plan(pb);
    fftw_destroy_plan(px);

    fftw_free(in_a);
    fftw_free(in_b);
    fftw_free(out);
    fftw_free(out_a);
    fftw_free(out_b);

    fftw_cleanup();

    return;
}




/*===================================================*/



Peak findAbsolutePeak(double *signal, int n)
{
    Peak pk;
    pk.delta = 0;
    pk.max = abs(signal[0]);

    for(int i = 1;i<n;i++)
    {
        if(abs(signal[i]) > abs(pk.max))
        {
            pk.max = signal[i];
            pk.delta=i;

        }
    }

    return pk;

}



Peak findMaxPeak(double *signal, int n)
{
    Peak pk;
    pk.delta = 0;
    pk.max = signal[0];

    for(int i = 1;i<n;i++)
    {
        if(signal[i] > pk.max)
        {
            pk.max = signal[i];
            pk.delta=i;
        }
    }

    return pk;
}


Peak findMinPeak(double *signal, int n)
{
    Peak pk;
    pk.delta = 0;
    pk.max = signal[0];

    for(int i = 1;i<n;i++)
    {
        if(signal[i] < pk.max)
        {
            pk.max = signal[i];
            pk.delta=i;
        }
    }

    return pk;
}


//=======================================================//






