#include "Headers/freqseries.h"

FreqSeries::FreqSeries() : QSplineSeries()
{

}

/*
 void FreqSeries::newMagSeries(TransFct *tf, int n, int fmin, int fmax)
{
    //Construction d'une série avec une précision de n point par octave
    //axe des fréquences : f[i] = fmin * 2^(i/n)
    int fprec = tf->Fprec();
    double moy = 0;
    int j = 0;
    double f = 0;
    int jmin = 0;
    int jmax = 0;
   // double *x = tf->f();
   // double *y = tf->mag();


    //if (n==0)
    if (true)
    {
        for (int i=1; i<tf->nfft() ;i++)
        {
            this->append(tf->f()[i], tf->mag()[i]);
        }
        return;
    }


    for (int i=0; ;i++)
    {
        f= fmin * pow((double)2,(double)i/n);  //On augmente f par pas de n-ieme d'octave

        j = round( f  / fprec);  //On sélectionne la fréquence la plus proche
                                 //à partir de la fonction de transert calculée



        //On fait une moyenne autour de cette fréquence
        jmin = round( fmin * pow((double)2,(double)(2*i-1)/(2*n)) / fprec);
        jmax = round( fmin * pow((double)2,(double)(2*i+1)/(2*n)) / fprec);
        moy = 0;

        assert(jmax!=jmin);

        //On sort si on a dépassé la fréquence max
        if ((j > (tf->nfft()-1) ) || (f>fmax))
            break;


        for (int k = jmin; k < jmax; k++)
        {
            moy += tf->mag()[k];
        }
        moy /= (jmax-jmin);


        //On ajoute
        this->append(tf->f()[j], moy);

    }

}

//*/


 void FreqSeries::newMagSeries(TF *tf, double n, double fmin, double fmax)
{
    //Construction d'une série avec une précision de n point par octave
    //axe des fréquences : f[i] = fmin * 2^(i/n)

     // si n = 0 : pas de smoothing

    double fprec = tf->Fprec();     //précision frequentielle de la fft
    double moy = 0;
    int j = 0;              //index du tableau f()
    double f = 0;           //fréquence centrale
    double jmin = 0;        //index minimal pour faire la moyenne
    double jmax = 0;        //index maximal
   // double *x = tf->f();
   // double *y = tf->mag();


    //test si la précision est suffisante?
    if(fprec>fmin)
        n=0;

    if (n==0)           //pas de smoothing
    {
        for (int i=1; i<tf->nfft() ;i++)
        {
            this->append(tf->f()[i], tf->mag()[i]);
        }
        return;
    }


    for (double i=0; ;i++)
    {
        f= fmin * pow((double)2,(double)i/n);  //On augmente f par pas de n-ieme d'octave
                                               // f = fmin * 2^(i/n)
        j = round( f  / fprec);  //On sélectionne la fréquence la plus proche
                                 //à partir de la fonction de transert calculée



        //On fait une moyenne autour de cette fréquence
        jmin = round( fmin * pow((double)2,(double)(2*i-1)/(2*n)) / fprec);   //= Fmin * 2^( (2i-1)/2n ) /fprec
        jmax = round( fmin * pow((double)2,(double)(2*i+1)/(2*n)) / fprec);   //= Fmin * 2^( (2i+1)/2n ) /fprec
        moy = 0;


        //### RISQUE DERREUR ICI SI LA PRECISION N'EST PAS SUFFISANTE ####
        assert(jmax!=jmin);

        //On sort si on a dépassé la fréquence max
        if ((jmax > (tf->nfft()-1) ) || (f>fmax))
            break;


        for (int k = jmin; k < jmax; k++)
        {
            moy += tf->mag()[k];
        }
        moy = moy / (jmax-jmin);


        //On ajoute
        this->append(tf->f()[j], moy);

    }

}


void FreqSeries::newPhsSeries(TF *tf, int n, int fmin, int fmax)
{
    //Construction d'une série avec une précision de n point par octave
    //axe des fréquences : f[i] = fmin * 2^(i/n)
    int fprec = tf->Fprec();
    double moy = 0;
    int j = 0;
    double f = 0;
    int jmin = 0;
    int jmax = 0;
    double *x = tf->f();
    double *y = tf->phs();


    if (n==0)
    {
        for (int i=1; i<tf->nfft() ;i++)
        {
            this->append(x[i], y[i]);
        }
        return;
    }


    for (int i=0; ;i++)
    {
        f= fmin * pow((double)2,(double)i/n);
        j = round( f  / fprec);  //On sélectionne la fréquence la plus proche
                                            //à partir de la fonction de transert calculée

        //On sort si on a dépassé la fréquence max
        if ((j > (tf->nfft()-1) ) || (f>fmax))
            break;

        //On fait une moyenne autour de cette fréquence
        jmin = round( fmin * pow((double)2,(double)(2*i-1)/(2*n)) / fprec);
        jmax = round( fmin * pow((double)2,(double)(2*i+1)/(2*n)) / fprec);
        moy = 0;
        for (int k = jmin; k < jmax; k++)
        {
            moy += y[k];
        }
        moy /= (jmax-jmin);


        //On ajoute
        this->append(x[j], moy);

    }


}
