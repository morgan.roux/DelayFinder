#include "Headers/mainwindow.h"
#include <QtWidgets>
#include "ui_mainwindow.h"
#include "Headers/dialogparam.h"
#include "dialogmeasure.h"

#define PI 3.14159265358979323846

MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent), ui(new Ui::MainWindow)
{
    initUI();

    connect(chartImp1, &ChartDrop::newFileDropped, this, &MainWindow::loadImpulse1);
    connect(chartImp2, &ChartDrop::newFileDropped, this, &MainWindow::loadImpulse2);
    connect(butGo, &QPushButton::clicked, this, &MainWindow::on_butGo_clicked);

}

MainWindow::~MainWindow()
{
    freememory();
    delete ui;
}

void MainWindow::initUI()
{
    ui->setupUi(this);

    w = new QWidget();
    lay = new QGridLayout();

    lbl_coordX = new QLabel("X = ");
    lbl_coordY = new QLabel("Y = ");

    lblDly = new QLabel();
    lblInfo = new QLabel();

    QFont font = lblInfo->font();
    font.setBold(true);
    lblInfo->setFont(font);

    butGo = new QPushButton();
    butGo->setText("Go");
    butGo->setMaximumWidth(200);
    butGo->setEnabled(false);

    vlay1 = new QVBoxLayout();
    vlay11 = new QVBoxLayout();
    vlay12 = new QVBoxLayout();
    vlay2 = new QVBoxLayout();
    vlay21 = new QVBoxLayout();
    hlay = new QGridLayout();

    grpImp = new QGroupBox("Normalized Impulses");
    grpXcorr = new QGroupBox("Processing");
    grpTf = new QGroupBox();


    chartImp1 = new ChartDrop(true);
    chartImp1->legend()->hide();
    chartImp1->setTheme(chartTheme);

    chartImp2 = new ChartDrop(true);
    chartImp2->legend()->hide();
    chartImp2->setTheme(chartTheme);

    chartXcorr = new ChartDrop(false);
    chartXcorr->legend()->hide();
   // chartXcorr->setTitle("Cross-Correlation");
    chartXcorr->setTheme(chartTheme);

    chart4 = new ChartDrop(false);
    chart4->setTheme(chartTheme);

    chart5 = new ChartDrop(false);
    chart5->legend()->hide();
    chart5->setTitle("Transfert Functions");
    chart5->setTheme(chartTheme);
/*
    chartImp1->addSeries(seriesImp1);
    chartImp2->addSeries(seriesImp2);
    chartXcorr->addSeries(seriesXcorr);


    chart4->addSeries(seriesTf1);
    chart4->addSeries(seriesTf2);
    chart4->addSeries(seriesTf3);

    chart5->addSeries(seriesTf4);
    chart5->addSeries(seriesTf5);
    chart5->addSeries(seriesTf6);
*/

    chartViewImp1 = new ChartViewTimeDomain(chartImp1);
    chartViewImp1->setRubberBand(QChartView::RectangleRubberBand);
    chartViewImp1->setRenderHint(QPainter::Antialiasing);

    chartViewImp2 = new ChartViewTimeDomain(chartImp2);
    chartViewImp2->setRubberBand(QChartView::RectangleRubberBand);
    chartViewImp2->setRenderHint(QPainter::Antialiasing);

    chartViewXCorr3 = new ChartViewTimeDomain(chartXcorr);
    chartViewXCorr3->setRubberBand(QChartView::RectangleRubberBand);
    chartViewXCorr3->setRenderHint(QPainter::Antialiasing);

    chartView4 = new QChartView(chart4);
    chartView4->setRubberBand(QChartView::RectangleRubberBand);
    chartView4->setRenderHint(QPainter::Antialiasing);

    chartView5 = new ChartViewFreqDomain(chart5);
    chartView5->setRubberBand(QChartView::RectangleRubberBand);
    chartView5->setRenderHint(QPainter::Antialiasing);

//*
    axis1X = new ValueAxis();
    axis1Y = new ValueAxis();
    chartImp1->addAxis(axis1X, Qt::AlignBottom);
    chartImp1->addAxis(axis1Y, Qt::AlignLeft);
    axis1X->setLabelFormat("%i");
    axis1Y->setLabelFormat("%+3.2f");
    axis1X->setTitleText("Times (samples)");
    axis1Y->setTitleText("Amplitude");
    axis1X->applyNiceNumbers();
    axis1Y->applyNiceNumbers();

    axis2X = new ValueAxis();
    axis2Y = new ValueAxis();
    chartImp2->addAxis(axis2X, Qt::AlignBottom);
    chartImp2->addAxis(axis2Y, Qt::AlignLeft);
    axis2X->setLabelFormat("%i");
    axis2Y->setLabelFormat("%+3.2f");
    axis2X->setTitleText("Times (samples)");
    axis2Y->setTitleText("Amplitude");
    axis2X->applyNiceNumbers();
    axis2Y->applyNiceNumbers();

    axis3X = new ValueAxis();
    axis3Y = new ValueAxis();
    chartXcorr->addAxis(axis3X, Qt::AlignBottom);
    chartXcorr->addAxis(axis3Y, Qt::AlignLeft);
    axis3X->setLabelFormat("%i");
    axis3Y->setLabelFormat("%+3.2f");
    axis3X->setTitleText("Delta (samples)");
    axis3Y->setTitleText("Amplitude");
    axis3X->applyNiceNumbers();
    axis3Y->applyNiceNumbers();

    axis5X = new QLogValueAxis();
    axis5Y = new ValueAxis();
    chart5->addAxis(axis5X, Qt::AlignBottom);
    chart5->addAxis(axis5Y, Qt::AlignLeft);
    axis5X->setLabelFormat("%i");
    axis5Y->setLabelFormat("%i");
    axis5X->setTitleText("Frequency (Hz)");
    axis5Y->setTitleText("Magnitude (dB)");
  //*


    axis5Y->applyNiceNumbers();


    vlay11->addWidget(chartViewImp1);
    vlay11->addWidget(chartViewImp2);
    vlay12->addWidget(chartViewXCorr3);
   // vlay21->addWidget(chartView4);
    vlay12->addWidget(chartView5);

    grpImp->setLayout(vlay11);
    grpXcorr->setLayout(vlay12);
    //grpTf->setLayout(vlay21);

    vlay1->addWidget(grpImp);
    vlay2->addWidget(grpXcorr);
   // vlay2->addWidget(grpTf);
    lay->addLayout(vlay1,1,1);
    lay->addLayout(vlay2,1,2);

    lay->addWidget(lblDly,2,1,Qt::AlignHCenter);
    lay->addWidget(lblInfo,3,1,Qt::AlignHCenter);
    lay->addWidget(butGo,3,2,Qt::AlignHCenter);

    hlay->addWidget(lbl_coordX,1,1,Qt::AlignHCenter);
    hlay->addWidget(lbl_coordY,1,2,Qt::AlignHCenter);
    lay->addLayout(hlay,2,2);

    w->setLayout(lay);

    this->setCentralWidget(w);


    imp1 = NULL;
    imp2 = NULL;

}

void MainWindow::freememory()
{

    seriesImp1->detachAxis(axis1X);
    seriesImp1->detachAxis(axis1Y);
    seriesImp2->detachAxis(axis2X);
    seriesImp2->detachAxis(axis2Y);
    seriesXcorr->detachAxis(axis3X);
    seriesXcorr->detachAxis(axis3Y);
    seriesTf4->detachAxis(axis5X);
    seriesTf4->detachAxis(axis5Y);
    seriesTf5->detachAxis(axis5X);
    seriesTf5->detachAxis(axis5Y);
    seriesTf6->detachAxis(axis5X);
    seriesTf6->detachAxis(axis5Y);

    //removeallseries efface aussi l'objet
    chartImp1->removeAllSeries();
    chartImp2->removeAllSeries();
    chartXcorr->removeAllSeries();
    chart5->removeAllSeries();

    delete tf1;
    delete tf2;
    delete tf3;
    delete tf4;
    delete tf5;
    delete tf6;
    delete imp4;
    delete imp5;
/*
    delete seriesImp1;
    delete seriesImp2;
    delete seriesXcorr;
    delete seriesTf4;
    delete seriesTf5;
    delete seriesTf6;
*/
}




void MainWindow::loadImpulse1(string strc)
{

   // const char* c = strc.c_str();


    //désactivation du bouton go, et réinitialisation de la classe si besoin
    butGo->setEnabled(false);
    if (imp1 != NULL)
    {
        delete imp1;
        imp1 = NULL;
    }

    try
    {
    imp1 = new Impulse(strc);
    }
    catch (int e)
    {   //Annulation de la création de l'objet
        return;
    }


    //Test de précision suffisante : précision fréquentielle  > 1/6oct à 20Hz = 2Hz
    if (imp1->getFs()/imp1->samples() > 2)
    {
        QMessageBox msgBox;
        msgBox.setText("Warning - Not enough precision. The impulse is too short.");
        msgBox.exec();
    }
    chartImp1->setTitle(QString(strc.c_str()));//QString(c));

    //Activation du bouton go si les deux impulses sont chargées et si les fs correspondent
    if((imp1 != NULL) && (imp2 != NULL))
    {
        if(imp1->getFs() == imp2->getFs())
        {
            butGo->setEnabled(true);
            m_fs = imp1->getFs();
        }
        else
        {
            chartImp1->setTitle("Frequency mismatch - Please restart the software");
            chartImp2->setTitle("Frequency mismatch - Please restart the software");
            QMessageBox msgBox;
            msgBox.setText("Frequency mismatch - Please restart the software");
            msgBox.exec();

        }
    }

   // chartImp1->setAcceptDrops(false);

    return;
}

void MainWindow::loadImpulse2(string strc)
{


    // const char* c = strc.c_str();
    //const char* c = strc.c_str();

    //désactivation du bouton go, et réinitialisation de la classe si besoin
    butGo->setEnabled(false);

    if (imp2 != NULL)
    {
        delete imp2;
        imp2 = NULL;
    }

    try
    {
    imp2 = new Impulse(strc);
    }
    catch (int e)
    {
        return;
    }


    //Test de précision suffisante : précision fréquentielle  > 1/6oct à 20Hz = 2Hz
    if ((double)imp2->getFs()/ (double)imp2->samples() > 2 )
    {
        QMessageBox msgBox;
        msgBox.setText("Warning - Not enough precision. The impulse is too short.");
        msgBox.exec();
    }

    chartImp2->setTitle(QString(strc.c_str()));

    //Activation du bouton go si les deux impulses sont chargées et si les fs correspondent
    if((imp1 != NULL) && (imp2 != NULL))
    {
        if(imp1->getFs() == imp2->getFs())
        {
            butGo->setEnabled(true);
            m_fs = imp1->getFs();
        }
        else
        {
            chartImp1->setTitle("Frequency mismatch - Please restart the software");
            chartImp2->setTitle("Frequency mismatch - Please restart the software");
            QMessageBox msgBox;
            msgBox.setText("Frequency mismatch - Please restart the software");
            msgBox.exec();
        }
    }

 //   chartImp2->setAcceptDrops(false);

    return;

}

void MainWindow::on_butGo_clicked()
{

    butGo->setEnabled(false);

    if (go == true)    //Ce n'est pas la première fois que l'on clique sur go, donc on reinitialise
    {
        disconnect(chartViewImp1, &ChartViewTimeDomain::newXY, this, &MainWindow::updateXYTimeDomain);
        disconnect(chartViewImp2, &ChartViewTimeDomain::newXY, this, &MainWindow::updateXYTimeDomain);
        disconnect(chartViewXCorr3, &ChartViewTimeDomain::newXY, this, &MainWindow::updateXYTimeDomain);
        disconnect(chartView5, &ChartViewFreqDomain::newXY, this, &MainWindow::updateXYFreqDomain);
        chartViewImp1->setMouseEnabled(false);
        chartViewImp2->setMouseEnabled(false);
        chartViewXCorr3->setMouseEnabled(false);
        chartView5->setMouseEnabled(false);

        freememory();
    }
    else
        go = true;

    tf1 = new TF(imp1->getFs(),imp1->samples());
    tf2 = new TF(imp2->getFs(),imp2->samples());


    /* ============== FFT ==================*/

    fft(imp1,tf1);
    fft(imp2,tf2);
    tf3 = new TF(*tf1);
    tf3->sum(tf2);

/*
    chart4->removeAllSeries();
    seriesTf1 = new FreqSeries();
    seriesTf1->newMagSeries(tf1,6,20,10000);
    seriesTf2 = new FreqSeries();
    seriesTf2->newMagSeries(tf2,6,20,10000);
    seriesTf3 = new FreqSeries();
    seriesTf3->newMagSeries(tf3,6,20,10000);

    chart4->addSeries(seriesTf1);
    chart4->addSeries(seriesTf2);
    chart4->addSeries(seriesTf3);

    axis4X = new QLogValueAxis();
    axis4Y = new ValueAxis();
    chart4->createLogAxis(30,100,axis4X,axis4Y);
    chart4->attachAxis(seriesTf1,axis4X,axis4Y);
    chart4->attachAxis(seriesTf2,axis4X,axis4Y);
    chart4->attachAxis(seriesTf3,axis4X,axis4Y);

//*/

    /*=================== XCorr =======================*/
    xc = new double[imp1->samples() *2 - 1];    //valeur du calcul de cross correlation
    lags = new double[imp1->samples() *2 - 1];  //retard en samples


    for (int l = 0; l< imp1->m_samples *2 -1; l++)
         lags[l] = double(l)-double(imp1->m_samples-1);

    xCorr(imp1->getY(),imp2->getY(),xc,imp1->samples());

    /*================ Calcul du delay =================*/
    // pk.delta < 0 => imp1 en retard de "dsamples" samples.

    switch(m_phase)
    {
    case PHASE_AUTO :
         pk = findAbsolutePeak(xc,imp1->samples() *2 -1);
         break;
    case PHASE_IN :
         pk = findMaxPeak(xc, imp1->samples() *2 -1);
         break;
    case PHASE_OUT :
         pk = findMinPeak(xc, imp1->samples() *2 -1);
         break;
    }

    imp4 = new Impulse(*imp1);
    imp5 = new Impulse(*imp2);

//*
    dsamples = std::abs(pk.delta - (imp1->samples() - 1));

    if(lags[pk.delta] > 0)
        imp5->timeShift(dsamples);

    else
        imp4->timeShift(dsamples);
//*/



    //Oppo de phase ?
    if(pk.max<0)
        imp5->antiphase();


    //Calcul des fft des nouvelles impulses (avec correction de la dérive de phase ?)
    tf4 = new TF(imp4->getFs(),imp4->samples());
    tf5 = new TF(imp5->getFs(),imp5->samples());
    fft(imp4,tf4);
    fft(imp5,tf5);
/*
    //Correction de la dérive de phase à cause du delay
    Peak pk1 = findPeak(imp4->getY(),imp4->samples());
    Peak pk2 = findPeak(imp5->getY(),imp5->samples());

    int drv = std::min(pk1.delta,pk2.delta);
    tf4->timeShift(-imp4->getX()[drv]);
    tf5->timeShift(-imp4->getX()[drv]);

//*/

//*

    tf6 = new TF(*tf4);
    tf6->sum(tf5);

    //Normalisation de imp4 et imp5 pour l'affichage
    imp4->normalize();
    imp5->normalize();

    //On cherche l'amplitude min et max


//*/


    /*================ Affichage du résultat =================*/


    QPen pen;

    //-----------Texte-----------//
    QString str = "Delta = ";
    str.append(QString::number(lags[pk.delta]));
    str.append(" samples / ");
    str.append(QString::number(lags[pk.delta] * 1000.0/double(imp1->getFs()) ) );
    str.append(" ms @ ");
    str.append(QString::number(imp1->getFs() ));
    str.append(" Hz  -  Peak = ");
    str.append(QString::number(pk.max));

    lblDly->setText(str);

    str.clear();
    if(lags[pk.delta] >= 0)
        str = "You should delay B of ";
    else
        str = "You should delay A of ";

    str.append(QString::number(abs(lags[pk.delta] * 1000.0/double(imp1->getFs())) ) );


    if(pk.max<0)
        str.append(" ms, OUT OF PHASE.");
    else
        str.append(" ms, IN PHASE");

    lblInfo->setText(str);

    //-----------1ere impulse (après delay et déphasage) ------------//
    seriesImp1 = new TimeSeries(imp4->getX(),imp4->getY(), imp4->samples());
    chartImp1->addSeries(seriesImp1);
    seriesImp1->attachAxis(axis1X);
    seriesImp1->attachAxis(axis1Y);


  /*  axis1X->setMin(0);
    axis1X->setMax(imp4->samples());
    axis1Y->setMin(findMinPeak(imp4->getY(),imp4->samples()).max);
    axis1Y->setMax(findMaxPeak(imp4->getY(),imp4->samples()).max);
*/
    axis1X->m_min=0;
    axis1X->m_max=imp4->samples();
    axis1Y->m_min = findMinPeak(imp4->getY(),imp4->samples()).max;
    axis1Y->m_max = findMaxPeak(imp4->getY(),imp4->samples()).max;

    axis1X->resize();
    axis1Y->resize();

    axis1X->applyNiceNumbers();
    axis1Y->applyNiceNumbers();

    pen = seriesImp1->pen();
    pen.setWidth(1);
    pen.setColor(QColor(32,159,223));
    seriesImp1->setPen(pen);


    //----------Impulse 2 (après delay et déphasage)--------------//
    seriesImp2 = new TimeSeries(imp5->getX(),imp5->getY(), imp5->samples());
    chartImp2->addSeries(seriesImp2);
    seriesImp2->attachAxis(axis2X);
    seriesImp2->attachAxis(axis2Y);


    axis2X->m_min=0;
    axis2X->m_max=imp5->samples();
    axis2Y->m_min = findMinPeak(imp5->getY(),imp5->samples()).max;
    axis2Y->m_max = findMaxPeak(imp5->getY(),imp5->samples()).max;

    axis2X->resize();
    axis2Y->resize();

    axis2X->applyNiceNumbers();
    axis2Y->applyNiceNumbers();

    pen = seriesImp2->pen();
    pen.setWidth(1);
    pen.setColor(QColor(152,202,83));
    seriesImp2->setPen(pen);

    //----------Xcorr----------------//
    seriesXcorr = new TimeSeries(lags,xc,  imp1->samples()*2-1);
    chartXcorr->addSeries(seriesXcorr);
    seriesXcorr->attachAxis(axis3X);
    seriesXcorr->attachAxis(axis3Y);

    axis3X->m_min = -imp1->samples();
    axis3X->m_max = imp1->samples();
    axis3Y->m_min = findMinPeak(xc,imp1->samples()*2-1).max;
    axis3Y->m_max = findMaxPeak(xc,imp1->samples()*2-1).max;

    axis3X->resize();
    axis3Y->resize();

    axis3X->applyNiceNumbers();
    axis3Y->applyNiceNumbers();

    pen = seriesXcorr->pen();
    pen.setWidth(1);
    pen.setColor(QColor(248,50,122));
    seriesXcorr->setPen(pen);





    //--------------Phase---------------//
   /*
    chart4->removeAllSeries();
    seriesTf1 = new FreqSeries();
    seriesTf1->newPhsSeries(tf4,24,20,10000);
    seriesTf2 = new FreqSeries();
    seriesTf2->newPhsSeries(tf5,24,20,10000);
    seriesTf3 = new FreqSeries();
    seriesTf3->newPhsSeries(tf6,24,20,10000);

    chart4->addSeries(seriesTf1);
    chart4->addSeries(seriesTf2);
    chart4->addSeries(seriesTf3);

    axis4X = new QLogValueAxis();
    axis4Y = new ValueAxis();
    chart4->addAxis(axis4X, Qt::AlignBottom);
    chart4->addAxis(axis4Y, Qt::AlignLeft);
    axis4X->setTitleText("Data point");
    axis4X->setLabelFormat("%i");
    axis4X->setMin(20);
    axis4X->setMax(120);
    axis4X->setBase(10);
    axis4X->setMinorTickCount(-1);
    axis4Y->setMin(-PI);
    axis4Y->setMax(PI);
    seriesTf1->attachAxis(axis4X);
    seriesTf1->attachAxis(axis4Y);
    seriesTf2->attachAxis(axis4X);
    seriesTf2->attachAxis(axis4Y);
    seriesTf3->attachAxis(axis4X);
    seriesTf4->attachAxis(axis4Y);
//*/

    //-------------Magnitude-----------------//
//*
   //Test de la précision fréquentielle pour affichage en 1/6e d'octave
    int div;

 /*
if((tf4->Fprec()< 2) && (tf5->Fprec() < 2))
        div = 6;
    else
        div = 0;
//*/
    div=6;

    seriesTf4 = new FreqSeries();
    seriesTf4->newMagSeries(tf4,div,20,20000);
    seriesTf5 = new FreqSeries();
    seriesTf5->newMagSeries(tf5,div,20,20000);
    seriesTf6 = new FreqSeries();
    seriesTf6->newMagSeries(tf6,div,20,20000);


    chart5->addSeries(seriesTf4);
    chart5->addSeries(seriesTf5);
    chart5->addSeries(seriesTf6);
    seriesTf4->attachAxis(axis5X);
    seriesTf4->attachAxis(axis5Y);
    seriesTf5->attachAxis(axis5X);
    seriesTf5->attachAxis(axis5Y);
    seriesTf6->attachAxis(axis5X);
    seriesTf6->attachAxis(axis5Y);

    axis5X->setMin(m_fmin);
    axis5X->setMax(m_fmax);
    axis5X->setBase(10);
    axis5X->setMinorTickCount(-1);
    axis5Y->setMin(m_mmin);
    axis5Y->setMax(m_mmax);

    axis5Y->applyNiceNumbers();


    pen = seriesTf4->pen();
    pen.setWidth(2);
    pen.setColor(QColor(32,159,223));
    seriesTf4->setPen(pen);

    pen = seriesTf5->pen();
    pen.setWidth(2);
    pen.setColor(QColor(152,202,83));
    seriesTf5->setPen(pen);

    pen = seriesTf6->pen();
    pen.setWidth(2);
    pen.setColor(QColor(246,166,36));
    seriesTf6->setPen(pen);
//*/

    //------------------- Autorisation des actions utilisateurs -----------------------//
   //*
    connect(chartViewImp1, &ChartViewTimeDomain::newXY, this, &MainWindow::updateXYTimeDomain);
    connect(chartViewImp2, &ChartViewTimeDomain::newXY, this, &MainWindow::updateXYTimeDomain);
    connect(chartViewXCorr3, &ChartViewTimeDomain::newXY, this, &MainWindow::updateXYTimeDomain);
    connect(chartView5, &ChartViewFreqDomain::newXY, this, &MainWindow::updateXYFreqDomain);
    //*/
    chartViewImp1->setMouseEnabled(true);
    chartViewImp2->setMouseEnabled(true);
    chartViewXCorr3->setMouseEnabled(true);
    chartView5->setMouseEnabled(true);

    connect(axis1X,&ValueAxis::rangeChanged, axis1X,&ValueAxis::onRangeChanged);
    connect(axis1Y,&ValueAxis::rangeChanged, axis1Y,&ValueAxis::onRangeChanged);
    connect(axis2X,&ValueAxis::rangeChanged, axis2X,&ValueAxis::onRangeChanged);
    connect(axis2Y,&ValueAxis::rangeChanged, axis2Y,&ValueAxis::onRangeChanged);
    connect(axis3X,&ValueAxis::rangeChanged, axis3X,&ValueAxis::onRangeChanged);
    connect(axis3Y,&ValueAxis::rangeChanged, axis3Y,&ValueAxis::onRangeChanged);
    connect(axis5Y,&ValueAxis::rangeChanged, axis5Y,&ValueAxis::onRangeChanged);
    connect(axis1X,&ValueAxis::rangeChanged, axis2X,&ValueAxis::updateSize);
   // connect(axis2X,&ValueAxis::rangeChanged, axis1X,&ValueAxis::updateSize);

    butGo->setEnabled(true);

    return;
}





void MainWindow::updateXYTimeDomain(double xVal, double yVal)
{
    lbl_coordX->setText(QString("X = %1 samples / %2 ms @ %3Hz")
                        .arg(xVal, 1, 'f', 0, '0')
                        .arg(1000.0*xVal/double(m_fs),1,'f',1,'0')
                        .arg(m_fs));

    lbl_coordY->setText(QString("Y = %1").arg(yVal, 1, 'e', 1, '0'));
}

void MainWindow::updateXYFreqDomain(double xVal, double yVal)
{

    lbl_coordX->setText(QString("X = %1 Hz").arg(xVal, 1, 'f', 0, '0'));

    lbl_coordY->setText(QString("Y = %1 dB").arg(yVal, 1, 'f', 1, '0'));

}


void MainWindow::on_actionPref_triggered()
{
    DialogParam *dlg = new DialogParam(this);
    dlg->setWindowTitle("Preferences");
    dlg->exec();

}

void MainWindow::on_actionAbout_triggered()
{
    DialogAbout *dlg = new DialogAbout(this);
    dlg->setWindowTitle("About");
    dlg->exec();

}

void MainWindow::on_actionMeasure_triggered()
{
    DialogMeasure *dlg = new DialogMeasure(this);
    dlg->setWindowTitle("New Measure");
    if(dlg->exec() ==  QDialog::Accepted)
    {
        int impNumber = dlg->getImpNumber();
        string flnm = dlg->getFileName();
        delete dlg;

    //impulse n°1 ou 2 ?
//*
    if (impNumber == 1)
        loadImpulse1(flnm);
    else
        loadImpulse2(flnm);
//*/
    }



}


int MainWindow::mmax() const
{
    return m_mmax;
}

void MainWindow::setMmax(int mmax)
{
    m_mmax = mmax;
}

int MainWindow::mmin() const
{
    return m_mmin;
}

void MainWindow::setMmin(int mmin)
{
    m_mmin = mmin;
}

int MainWindow::phase() const
{
    return m_phase;
}

void MainWindow::setPhase(int phase)
{
    m_phase = phase;
}

int MainWindow::fmin() const
{
    return m_fmin;
}

void MainWindow::setFmin(int fmin)
{
    m_fmin = fmin;
}

int MainWindow::fmax() const
{
    return m_fmax;
}

void MainWindow::setFmax(int fmax)
{
    m_fmax = fmax;
}

int MainWindow::fs() const
{
    return m_fs;
}

void MainWindow::setFs(int fs)
{
    m_fs = fs;
}


