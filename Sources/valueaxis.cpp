#include "Headers/valueaxis.h"

ValueAxis::ValueAxis() : QValueAxis()
{
    m_min= -1;
    m_max = 1;
    //connect(this,&ValueAxis::rangeChanged, this,&ValueAxis::applyNiceNumbers);
}

void ValueAxis::resize()
{
    setMin(m_min);
    setMax(m_max);
    return;
}

void ValueAxis::applyNiceNumbers()
{
    this->setTickCount(10);
    QValueAxis::applyNiceNumbers();

}

void ValueAxis::onRangeChanged(qreal min, qreal max)
{
  /*
    if(min<m_min)
        setMin(m_min);
    if(max>m_max)
        setMax(m_max);
//*/

  //  this->setTickCount(10);
    //QValueAxis::applyNiceNumbers();

}

void ValueAxis::updateSize(qreal min, qreal max)
{
    setMin(min);
    setMax(max);
    return;

}

