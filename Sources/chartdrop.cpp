#include "Headers/chartdrop.h"
#define PI 3.14159265358979323846

ChartDrop::ChartDrop(bool b) : QChart()
{
    this->setAcceptDrops(b);
}

void ChartDrop::dropEvent(QGraphicsSceneDragDropEvent* event)
 {

    if (event->mimeData()->hasUrls())
    {

        event->acceptProposedAction();
        QList<QUrl> list = event->mimeData()->urls();
        QString qstr = list[0].toString();

        //MAC :
        emit newFileDropped(qstr.toStdString().substr(7));
        //PC :
       // emit newFileDropped(str.toStdString().substr(8));

        return;


    }
}


/*
void ChartDrop::addNewSeries(double *x, double *y, int l, QLineSeries *series)
{

    for (int i=0;i<l;i++)
        series->append(x[i], y[i]);


    this->addSeries(series);


}

void ChartDrop::createLogAxis(int Xmin, int Xmax, QLogValueAxis *axisX, QValueAxis *axisY)
{

    this->addAxis(axisX, Qt::AlignBottom);
    this->addAxis(axisY, Qt::AlignLeft);
    axisX->setTitleText("Data point");
    axisX->setLabelFormat("%i");
    axisX->setMin(Xmin);
    axisX->setMax(Xmax);
    axisX->setBase(10);
    axisX->setMinorTickCount(-1);
   // axisX->setTickCount(series1->count());
    axisY->setTitleText("Values");
    axisY->setLabelFormat("%g");
    //  axisX->setTickCount(series1->count());

}

void ChartDrop::createLinAxis(int Xmin, int Xmax, QValueAxis *axisX, QValueAxis *axisY)
{

    this->addAxis(axisX, Qt::AlignBottom);
    this->addAxis(axisY, Qt::AlignLeft);

    axisX->setTitleText("Data point");
    axisX->setLabelFormat("%i");
   // axisX->setTickCount(series1->count());

    axisY->setTitleText("Values");
    axisY->setLabelFormat("%g");
    //  axisX->setTickCount(series1->count());

}
/*
void ChartDrop::attachAxis(QLineSeries *series, QAbstractAxis *axisX, QAbstractAxis *axisY)
{
    series->attachAxis(axisX);
    series->attachAxis(axisY);

}
*/

//======================================================//

