#include "Headers/chartview.h"


ChartView::ChartView(QChart *chart) : QChartView(chart)
{

}


bool ChartView::getMouseEnabled() const
{
    return mouseEnabled;
}

void ChartView::setMouseEnabled(bool value)
{
    mouseEnabled = value;
}

void ChartView::keyPressEvent(QKeyEvent *event)
{
    QValueAxis* axis;

    switch (event->key())
    {
    case Qt::Key_Plus:
        chart()->zoomIn();
        break;

    case Qt::Key_Minus:
        chart()->zoomOut();
        break;

    case Qt::Key_Left:
        chart()->scroll(-10,0);
        break;

    case Qt::Key_Right:
        chart()->scroll(10,0);
        break;

    case Qt::Key_Up:
        chart()->scroll(0,10);
        break;

    case Qt::Key_Down:
        chart()->scroll(0,-10);
        break;


    }


}

void ChartView::mouseDoubleClickEvent(QMouseEvent *event)
{
    if(event->button() == Qt::LeftButton)
        chart()->zoomReset();

    QChartView::mouseDoubleClickEvent(event);

}

//==============================================================//

ChartViewTimeDomain::ChartViewTimeDomain(QChart *chart) : ChartView(chart)
{

}

void ChartViewTimeDomain::mouseMoveEvent(QMouseEvent *event)
{
    if(mouseEnabled)
    {
        /*
        QLineSeries* ser = (QLineSeries*) this->chart()->series()[0];

        assert(ser!=NULL);

        double index = round(this->chart()->mapToValue(event->pos()).x()) - ser->at(0).x();

        if (index<0)
            index=0;

        double xVal = ser->at(index).x(); //ser->at(index).x();
        double yVal = ser->at(index).y();
//*/
        double xVal = this->chart()->mapToValue(event->pos()).x();
        double yVal = this->chart()->mapToValue(event->pos()).y();

        if(yVal >= -1 && yVal <= 1 && xVal > -50000 && xVal < 50000 )
            emit newXY(xVal,yVal);

     }

     QChartView::mouseMoveEvent(event);

}



//=======================================================//

ChartViewFreqDomain::ChartViewFreqDomain(QChart *chart) : ChartView(chart)
{

}

void ChartViewFreqDomain::mouseMoveEvent(QMouseEvent *event)
{
    if(mouseEnabled)
    {
        /*
        QLineSeries* ser = (QLineSeries*) this->chart()->series()[0];

        assert(ser!=NULL);

        double index = round(this->chart()->mapToValue(event->pos()).x()) - ser->at(0).x();

        if (index<0)
            index=0;

        double xVal = ser->at(index).x(); //ser->at(index).x();
        double yVal = ser->at(index).y();

        if(yVal>-100 && yVal<30 && xVal>0 && xVal < 20000 )
            emit newXY(xVal,yVal);
            //*/
      //*

        double xVal = this->chart()->mapToValue(event->pos()).x();
        double yVal = this->chart()->mapToValue(event->pos()).y();

        if(yVal>-100 && yVal<30 && xVal>0 && xVal < 20000 )
             emit newXY(xVal, yVal);

        //*/

     }

     QChartView::mouseMoveEvent(event);

}
